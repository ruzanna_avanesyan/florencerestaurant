<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
	'timeZone' => 'Asia/Yerevan',
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'My Web Application',

	// preloading 'log' component
	'preload'=>array('log'),

	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.components.*',
	),

	'modules'=>array(
		// uncomment the following to enable the Gii tool

		'gii'=>array(
			'class'=>'system.gii.GiiModule',
			'password'=>'123123',
			// If removed, Gii defaults to localhost only. Edit carefully to taste.
			'ipFilters'=>array('127.0.0.1','::1'),
		),
		'staff',

	),

	// application components
	'components'=>array(

		'user'=>array(
			// enable cookie-based authentication
			'allowAutoLogin'=>true,
		),

		// uncomment the following to enable URLs in path-format

		'urlManager'=>array(
			'urlFormat'=>'path',
			'rules'=>array(
				'<controller:\w+>/<id:\d+>'=>'<controller>/view',
				'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
				'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
			),
		),
//
//		'urlManager'=>array(
//			'class'=>'application.components.UrlManager',
//			'urlFormat'=>'path',
//			'showScriptName'=>false,
//			'rules'=>array(
//				'<language:(ru|am|en)>/' => 'site/index',
//				'<language:(ru|am|en)>/<action:(contact|webdesign|webdevelopment|mobiledevelopment|smm|graphicdesign|aboutus)>/*' => 'site/<action>',
//				//'<language:(ru|am|en)>/<action:(list)>/*' => 'portfolio/<action>',
//				'<language:(ru|am|en)>/<controller:\w+>/<id:\d+>'=>'<controller>/view',
//				'<language:(ru|am|en)>/<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
//				'<language:(ru|am|en)>/<controller:\w+>/<action:\w+>/*'=>'<controller>/<action>',
//			),
//		),


		// database settings are configured in database.php
		'db'=>require(dirname(__FILE__).'/database.php'),


		'errorHandler'=>array(
			// use 'site/error' action to display errors
			'errorAction'=>YII_DEBUG ? null : 'site/error',
		),

		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
				// uncomment the following to show log messages on web pages
				/*
				array(
					'class'=>'CWebLogRoute',
				),
				*/
			),
		),
		'widgetFactory' => array(
			'widgets' => array(
				'CLinkPager' => array(
					'htmlOptions' => array(
						'class' => 'pagination'
					),
					'header' => false,
					'maxButtonCount' => 5,
					'cssFile' => false,
				),
				'CGridView' => array(
					'htmlOptions' => array(
						'class' => 'table-responsive'
					),
					'pagerCssClass' => 'dataTables_paginate paging_bootstrap',
					'itemsCssClass' => 'table table-bordered table-hover',
					'cssFile' => false,
					'summaryCssClass' => 'dataTables_info',
					'summaryText' => 'Showing {start} to {end} of {count} entries',
					'template' => '{items}<div class="row"><div class="col-md-5 col-sm-12">{summary}</div><div class="col-md-7 col-sm-12">{pager}</div></div><br />',
				),
			),
		),
		array(
			'class'   => 'CButtonColumn',
			'buttons' => array(
				'view'   => array(
					'options'  => array('rel' => 'tooltip', 'data-toggle' => 'tooltip', 'title' => Yii::t('app', 'View')),
					'label'    => '<button class="btn btn-sm btn-info"><i class="fa fa-eye"></i></button>',
					'imageUrl' => false,
				),
				'update' => array(
					'options'  => array('rel' => 'tooltip', 'data-toggle' => 'tooltip', 'title' => Yii::t('app', 'Update')),
					'label'    => '<button class="btn btn-sm btn-primary"><i class="fa fa-pencil"></i></button>',
					'imageUrl' => false,
				),
				'delete' => array(
					'options'  => array('rel' => 'tooltip', 'data-toggle' => 'tooltip', 'title' => Yii::t('app', 'Delete')),
					'label'    => '<button class="btn btn-sm btn-danger"><i class="fa fa-times"></i></button>',
					'imageUrl' => false,
				)
			)
		),
		'session' => array(
			'autoStart'=>true,
		),

	),
	'sourceLanguage'=>'en',
	'language'=>'en',
	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>array(
		// this is used in contact page
		'adminEmail'=>'aeblocks1111@gmail.com',
		'password'=>'aeblocks1111aeblocks1111aeblocks1111aeblocks1111',
		'smtp' => 'smtp.gmail.com',
	),
);

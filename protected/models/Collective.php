<?php

/**
 * This is the model class for table "collective".
 *
 * The followings are the available columns in table 'collective':
 * @property string $id
 * @property string $image
 * @property integer $sort_order
 *
 * The followings are the available model relations:
 * @property CollectiveLabel[] $collectiveLabels
 */
class Collective extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public $first_name;

	public function tableName()
	{
		return 'collective';
	}

	public function getFirst_name(){
		if(isset($this->first_name)){
			return $this->first_name;
		}else{
			return null;
		}
	}

	public function setFirst_name($val){
		$this->first_name = $val;
	}
	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('image', 'required'),
			array('sort_order', 'numerical', 'integerOnly'=>true),
			array('image', 'length', 'max'=>250),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, image, sort_order, first_name', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'collectiveLabels' => array(self::HAS_MANY, 'CollectiveLabel', 'collective_id'),
			'collectiveLabel' => array(self::HAS_ONE, 'CollectiveLabel', 'collective_id'),
			'collectiveExperiences' => array(self::HAS_MANY, 'CollectiveExperience', 'collective_id'),

		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'image' => 'Image',
			'sort_order' => 'Sort Order',
			'first_name' => 'First Name',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->with= array('collectiveLabel');
		$criteria->together = true;
		$criteria->compare('collectiveLabel.first_name',$this->first_name,true);

		$criteria->compare('t.id',$this->id,true);
		$criteria->compare('t.image',$this->image,true);
		$criteria->compare('t.sort_order',$this->sort_order);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort'=>array(
				'attributes'=>array(
					'first_name'=>array(
						'asc'=>'first_name',
						'desc'=>'first_name DESC',
					),
					'*',
				),
			),
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Collective the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}

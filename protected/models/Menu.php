<?php

/**
 * This is the model class for table "menu".
 *
 * The followings are the available columns in table 'menu':
 * @property string $id
 * @property integer $sort_order
 * @property string $url
 *
 * The followings are the available model relations:
 * @property MenuLabel[] $menuLabels
 */
class Menu extends CActiveRecord
{

	public $value;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'menu';
	}

	public function getValue(){
		if(isset($this->value)){
			return $this->value;
		}else{
			return null;
		}
	}

	public function setValue($val){
		$this->value = $val;
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('url', 'required'),
			array('sort_order', 'numerical', 'integerOnly'=>true),
			array('url', 'length', 'max'=>250),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, sort_order, url, value', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'menuLabels' => array(self::HAS_MANY, 'MenuLabel', 'menu_id'),
			'menuLabel' => array(self::HAS_ONE, 'MenuLabel', 'menu_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'sort_order' => 'Sort Order',
			'url' => 'Url',
			'value' => 'Value',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		$criteria->with= array('menuLabel');
		$criteria->together = true;
		$criteria->compare('menuLabel.value',$this->value,true);
		$criteria->compare('t.id',$this->id,true);
		$criteria->compare('t.sort_order',$this->sort_order);
		$criteria->compare('t.url',$this->url,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort'=>array(
				'attributes'=>array(
					'value'=>array(
						'asc'=>'value',
						'desc'=>'value DESC',
					),
					'*',
				),
			),
			'pagination'=>array(
				'pageSize'=>50,
			),
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Menu the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}

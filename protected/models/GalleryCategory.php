<?php

/**
 * This is the model class for table "gallery_category".
 *
 * The followings are the available columns in table 'gallery_category':
 * @property string $id
 * @property string $sort_order
 *
 * The followings are the available model relations:
 * @property Gallery[] $galleries
 * @property GalleryCategoryLabel[] $galleryCategoryLabels
 */
class GalleryCategory extends CActiveRecord
{

	public $name;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'gallery_category';
	}

	public function getName(){
		if(isset($this->name)){
			return $this->name;
		}else{
			return null;
		}
	}

	public function setName($val){
		$this->name = $val;
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('sort_order', 'length', 'max'=>11),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, sort_order,name', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'galleries' => array(self::HAS_MANY, 'Gallery', 'category_id'),
			'galleryCategoryLabels' => array(self::HAS_MANY, 'GalleryCategoryLabel', 'gallery_category_id'),
			'galleryCategoryLabel' => array(self::HAS_ONE, 'GalleryCategoryLabel', 'gallery_category_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'sort_order' => 'Sort Order',
			'name' => 'Name',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->with= array('galleryCategoryLabel');
		$criteria->together = true;
		$criteria->compare('galleryCategoryLabel.name',$this->name,true);

		$criteria->compare('t.id',$this->id,true);
		$criteria->compare('t.sort_order',$this->sort_order,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort'=>array(
				'attributes'=>array(
					'name'=>array(
						'asc'=>'name',
						'desc'=>'name DESC',
					),
					'*',
				),
			),
			'pagination'=>array(
				'pageSize'=>50,
			),
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return GalleryCategory the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}

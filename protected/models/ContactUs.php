<?php

/**
 * This is the model class for table "contact_us".
 *
 * The followings are the available columns in table 'contact_us':
 * @property string $id
 * @property string $phone
 * @property string $email
 *
 * The followings are the available model relations:
 * @property ContactUsLabel[] $contactUsLabels
 */
class ContactUs extends CActiveRecord
{

	public $address;

	public function tableName()
	{
		return 'contact_us';
	}
	public function getAddress(){
		if(isset($this->address)){
			return $this->address;
		}else{
			return null;
		}
	}

	public function setAddress($val){
		$this->address = $val;
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('phone, email', 'length', 'max'=>250),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, phone, email,address', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'contactUsLabels' => array(self::HAS_MANY, 'ContactUsLabel', 'contact_us_id'),
			'contactUsLabel' => array(self::HAS_ONE, 'ContactUsLabel', 'contact_us_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'phone' => 'Phone',
			'email' => 'Email',
			'address' => 'Address',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		$criteria->with= array('contactUsLabel');
		$criteria->together = true;
		$criteria->compare('contactUsLabel.address',$this->address,true);
		$criteria->compare('t.id',$this->id,true);
		$criteria->compare('t.phone',$this->phone,true);
		$criteria->compare('t.email',$this->email,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort'=>array(
				'attributes'=>array(
					'address'=>array(
						'asc'=>'address',
						'desc'=>'address DESC',
					),
					'*',
				),
			),
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ContactUs the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}

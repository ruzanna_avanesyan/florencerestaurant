<?php
/* @var $this DontSendUserController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Dont Send Users',
);

$this->menu=array(
	array('label'=>'Create DontSendUser', 'url'=>array('create')),
	array('label'=>'Manage DontSendUser', 'url'=>array('admin')),
);
?>

<h1>Dont Send Users</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>

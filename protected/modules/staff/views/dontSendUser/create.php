<?php
/* @var $this DontSendUserController */
/* @var $model DontSendUser */

$this->breadcrumbs=array(
	'Dont Send Users'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List DontSendUser', 'url'=>array('index')),
	array('label'=>'Manage DontSendUser', 'url'=>array('admin')),
);
?>

<h1>Create DontSendUser</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>
<?php
/* @var $this DontSendUserController */
/* @var $model DontSendUser */

$this->breadcrumbs=array(
	'Dont Send Users'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List DontSendUser', 'url'=>array('index')),
	array('label'=>'Create DontSendUser', 'url'=>array('create')),
	array('label'=>'Update DontSendUser', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete DontSendUser', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage DontSendUser', 'url'=>array('admin')),
);
?>

<h1>View DontSendUser #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'email',
		'created_date',
	),
)); ?>

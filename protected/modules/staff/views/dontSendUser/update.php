<?php
/* @var $this DontSendUserController */
/* @var $model DontSendUser */

$this->breadcrumbs=array(
	'Dont Send Users'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List DontSendUser', 'url'=>array('index')),
	array('label'=>'Create DontSendUser', 'url'=>array('create')),
	array('label'=>'View DontSendUser', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage DontSendUser', 'url'=>array('admin')),
);
?>

<h1>Update DontSendUser <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>
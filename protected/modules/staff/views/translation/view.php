<section class="content-header">
	<h1>
		Translation
		<small>#<?php echo $model->id; ?></small>
	</h1>

</section>
<section class="content">
	<div class="row">
		<div class="col-xs-12">
			<div class="box">
				<div class="box-header">
					<h3 class="box-title">Translation #<?php echo $model->id; ?></h3>
					<a href="<?=$this->CreateUrl('Translation/update/id/'.$model->id)?>" class="btn btn-success pull-right" style="margin-left: 15px">Edit</a>
					<a href="<?=$this->CreateUrl('Translation/create')?>" class="btn btn-primary pull-right">Add New</a>
				</div>
				<!-- /.box-header -->
				<div class="box-body">

					<?php $this->widget('zii.widgets.CDetailView', array(
						'data'=>$model,
						'htmlOptions' => array('class' => 'table table-bordered table-hover'),
						'attributes'=>array(
							'id',
							'key',
							array(
								'name' 	=> 'value',
								'value'	=> Helpers::Name($model,'translationLabels','value'),
								'type' 	=> 'raw'
							),
						),
					)); ?>
				</div>
			</div>
		</div>
	</div>
</section>
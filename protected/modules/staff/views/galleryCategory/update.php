<?php
/* @var $this GalleryCategoryController */
/* @var $model GalleryCategory */

$this->breadcrumbs=array(
	'Gallery Categories'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List GalleryCategory', 'url'=>array('index')),
	array('label'=>'Create GalleryCategory', 'url'=>array('create')),
	array('label'=>'View GalleryCategory', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage GalleryCategory', 'url'=>array('admin')),
);
?>

<h1>Update GalleryCategory <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>
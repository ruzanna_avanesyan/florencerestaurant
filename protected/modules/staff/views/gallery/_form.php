<section class="content">
	<div class="row">
		<!-- left column -->
		<div class="col-md-12">
			<!-- general form elements -->
			<div class="box box-primary">
				<div class="box-header with-border">
					<h3 class="box-title"><p class="note">Fields with <span class="required">*</span> are required.</p></h3>
					<h3 class="box-title">Image size - 640x477</h3>
				</div>

					<?php $form=$this->beginWidget('CActiveForm', array(
						'id'=>'gallery-form',
						'enableAjaxValidation'=>false,
						'htmlOptions' => array(
							'class' =>'form-horizontal',
							"enctype" => "multipart/form-data"
						)
					)); ?>


						<?php echo $form->errorSummary($model); ?>

						<div class="form-group">
							<?php echo $form->labelEx($model,'category_id',array('class'=>'col-sm-2 control-label')); ?>
							<div class="col-sm-10">
								<select name="Gallery[category_id]" class="form-control">
								<?php
									if(!empty($category)):
										foreach($category as $key => $value):
								?>
											<option value="<?=$value->id?>"><?=$value->galleryCategoryLabel->name?></option>
								<?php
										endforeach;
									endif;
								?>
								</select>
							</div>
							<?php echo $form->error($model,'category_id'); ?>
						</div>
					<?php
						if($model->isNewRecord):
					?>
						<div class="form-group">
							<?php echo $form->labelEx($model,'image* (640x477)',array('class'=>'col-sm-2 control-label')); ?>
							<div class="col-sm-10">
								<?php echo $form->fileField($model,'image[]',array('size'=>60,'maxlength'=>250,'class' => 'form-control','multiple' => 'multiple')); ?>
							</div>
							<?php echo $form->error($model,'image'); ?>
						</div>
					<?php else:?>
							<div class="form-group">
								<?php echo $form->labelEx($model,'image',array('class'=>'col-sm-2 control-label')); ?>
								<div class="col-sm-10">
									<?php echo $form->fileField($model,'image',array('size'=>60,'maxlength'=>250,'class' => 'form-control')); ?>
								</div>
								<?php echo $form->error($model,'image'); ?>
							</div>

							<div class="form-group">
								<?php echo $form->labelEx($model,'sort_order',array('class'=>'col-sm-2 control-label')); ?>
								<div class="col-sm-10">
									<?php echo $form->textField($model,'sort_order',array('class' => 'form-control')); ?>
								</div>
								<?php echo $form->error($model,'sort_order'); ?>
							</div>
					<?php
						endif;
					?>


						<div class="form-group buttons">
							<div class="col-sm-12">
								<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',array('class'=>'btn btn-primary pull-right')); ?>
							</div>
						</div>

					<?php $this->endWidget(); ?>
			</div>
		</div>
	</div>
</section>
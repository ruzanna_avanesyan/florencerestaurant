
<section class="content-header">
	<h1>
		Gallery
	</h1>

</section>
<section class="content">
	<div class="row">
		<div class="col-xs-12">
			<div class="box">
				<div class="box-header">
					<h3 class="box-title">Gallery</h3>
					<a href="<?=$this->CreateUrl('Gallery/create')?>" class="btn btn-primary pull-right">Add New</a>
				</div>
				<!-- /.box-header -->
				<div class="box-body">

						<?php $this->widget('zii.widgets.grid.CGridView', array(
							'id'=>'gallery-grid',
							'dataProvider'=>$model->search(),
							'filter'=>$model,
							'columns'=>array(
								'id',
								array(
									'name'=>'image',
									'type' => 'raw',
									'htmlOptions'=>array(
										'class' => 'admin_img'
									),
									'value'=>'CHtml::image(Yii::app()->baseUrl . "/uploads/gallery/" . $data->image)'


								),
								array(
									'name' 	=> 'category_id',
									'filter' => CHTml::dropDownList("Gallery[category_id]",$model->category_id,
										CHtml::listData(GalleryCategoryLabel::model()->findAllByAttributes(array('language_id' =>1)), 'gallery_category_id', 'name'),
										array('empty' => 'All', 'class' => 'form-control')
									),
									'value'	=> '$data->category->galleryCategoryLabel->name'
								),
								'sort_order',
								array(
									'class' => 'zii.widgets.grid.CButtonColumn',
									'htmlOptions' => array('style' => 'white-space: nowrap'),
									'afterDelete' => 'function(link,success,data) { if (success && data) alert(data); }',
									'buttons' => array(
										'view' => array(
											'options' => array('rel' => 'tooltip', 'data-toggle' => 'tooltip', 'title' => Yii::t('app', 'View'),'class'=>'img-btn'),
											'label' => '<i class="fa fa-eye"></i>',
											'imageUrl' => false,
										),
										'update' => array(
											'options' => array('rel' => 'tooltip', 'data-toggle' => 'tooltip', 'title' => Yii::t('app', 'Update'),'class'=>'img-btn'),
											'label' => '<i class="fa fa-pencil"></i>',
											'imageUrl' => false,
										),
										'delete' => array(
											'options' => array('rel' => 'tooltip', 'data-toggle' => 'tooltip', 'title' => Yii::t('app', 'Delete'),'class'=>'img-btn del-btn'),
											'label' => '<i class="fa fa-times"></i>',
											'imageUrl' => false,
										)
									)
								),
							),
						)); ?>
				</div>
			</div>
		</div>
	</div>
</section>
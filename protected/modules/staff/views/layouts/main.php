
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>AdminLTE 2 | Dashboard</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="<?=Yii::app()->request->BaseUrl?>/vendor/assets/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?=Yii::app()->request->BaseUrl?>/vendor/assets/dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="<?=Yii::app()->request->BaseUrl?>/vendor/assets/dist/css/skins/_all-skins.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="<?=Yii::app()->request->BaseUrl?>/vendor/assets/iCheck/flat/blue.css">
    <!-- Morris chart -->
    <link rel="stylesheet" href="<?=Yii::app()->request->BaseUrl?>/vendor/assets/morris/morris.css">
    <!-- jvectormap -->
    <link rel="stylesheet" href="<?=Yii::app()->request->BaseUrl?>/vendor/assets/jvectormap/jquery-jvectormap-1.2.2.css">
    <!-- Date Picker -->
    <link rel="stylesheet" href="<?=Yii::app()->request->BaseUrl?>/vendor/assets/datepicker/jquery.simple-dtpicker.css">
    <link rel="stylesheet" href="<?=Yii::app()->request->BaseUrl?>/vendor/assets/datepicker/datepicker3.css">
    <!-- Daterange picker -->
    <link rel="stylesheet" href="<?=Yii::app()->request->BaseUrl?>/vendor/assets/daterangepicker/daterangepicker-bs3.css">
    <!-- bootstrap wysihtml5 - text editor -->
    <link rel="stylesheet" href="<?=Yii::app()->request->BaseUrl?>/vendor/assets/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- jQuery 2.1.4 -->
    <script src="<?=Yii::app()->request->BaseUrl?>/vendor/assets/js/jQuery-2.1.4.min.js"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <!-- daterangepicker -->
    <script src="<?=Yii::app()->request->BaseUrl?>/vendor/assets/js/style.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
    <script src="<?=Yii::app()->request->BaseUrl?>/vendor/assets/daterangepicker/daterangepicker.js"></script>
    <script src="<?=Yii::app()->request->BaseUrl?>/vendor/assets/js/admin.js"></script>
    <!-- datepicker -->

    <!-- CK Editor -->
    <!--	<script src="https://cdn.ckeditor.com/4.4.3/standard/ckeditor.js"></script>-->
    <script src="<?=Yii::app()->request->BaseUrl?>/vendor/assets/ckeditor/ckeditor.js"></script>
    <!-- Bootstrap WYSIHTML5 -->
    <script src="<?=Yii::app()->request->BaseUrl?>/vendor/assets/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
    <link rel="stylesheet" href="<?=Yii::app()->request->BaseUrl?>/vendor/assets/datetimepicker-master/jquery.datetimepicker.css">
    <script type='text/javascript' src='<?=Yii::app()->request->BaseUrl?>/vendor/assets/datetimepicker-master/jquery.datetimepicker.min.js'></script>
    <script type='text/javascript' src='<?=Yii::app()->request->BaseUrl?>/vendor/assets/datetimepicker-master/build/jquery.datetimepicker.full.js'></script>
    <link rel="stylesheet" href="<?=Yii::app()->request->BaseUrl?>/vendor/assets/css/admin_style.css">


</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <header class="main-header">
        <!-- Logo -->
        <a href="<?=$this->CreateUrl('admin/admin')?>" class="logo">
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg"><b>Florence Restaurant</b></span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                <span class="sr-only">Toggle navigation</span>
            </a>

            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">

                    <li>
                        <a href="<?=$this->CreateUrl('/staff/UserHasService/admin')?>" >
                            <i class="fa fa-certificate message_notif" aria-hidden="true"></i>
                            <span class="admin_msg_notif" id=""></span>
                        </a>
                    </li>
                    <li>
                        <a href="<?=$this->CreateUrl('/staff/Messages/admin')?>" >
                            <i class="fa fa-envelope message_notif" aria-hidden="true"></i>
                            <span class="admin_msg_notif" id=""></span>
                        </a>
                    </li>
                    <!-- User Account: style can be found in dropdown.less -->
                    <li class="dropdown user user-menu">

                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <img src="<?=Yii::app()->request->BaseUrl?>/vendor/assets/images/imgpsh_fullsize.png" class="user-image" alt="User Image">
                            <span class="hidden-xs"></span>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- User image -->
                            <li class="user-header">
                                <img src="<?=Yii::app()->request->BaseUrl?>/vendor/assets/images/imgpsh_fullsize.png" class="img-circle" alt="User Image">

                                <p>
                                    -Admin
                                </p>
                            </li>

                            <!-- Menu Footer-->
                            <li class="user-footer">
                                <div class="pull-left">
                                    <a href="" class="btn btn-default btn-flat">Profile</a>
                                </div>
                                <div class="pull-right">
                                    <a href="<?php echo $this->CreateUrl('/staff/default/logout'); ?>" class="btn btn-default btn-flat">Sign out</a>
                                </div>
                            </li>
                        </ul>
                    </li>
                    <!-- Control Sidebar Toggle Button -->
                    <li>
                        <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
                    </li>
                </ul>
            </div>
        </nav>
    </header>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
            <!-- Sidebar user panel -->
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="<?=Yii::app()->request->BaseUrl?>/vendor/assets/images/imgpsh_fullsize.png" class="img-circle" alt="User Image">
                </div>
                <div class="pull-left info">
                    <p></p>
                    <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                </div>
            </div>

            <!-- sidebar menu: : style can be found in sidebar.less -->
            <ul class="sidebar-menu nav">
                <li class="header">MAIN NAVIGATION</li>



                <li class="treeview stuff">
                    <a href="<?=$this->CreateUrl('/staff/admin/admin')?>">
                        <i class="fa fa-users"></i>
                        <span>Admins</span>
                    </a>
                </li>
                <li class="treeview foodmenu">
                    <a href="#">
                        <i class="fa fa-list"></i>
                        <span>Food Menu</span>
                    </a>
                    <ul class="treeview-menu">
                        <li class="foodmenu" ><a href="<?=$this->CreateUrl('/staff/FoodMenu/admin')?>"><i class="fa fa-circle-o"></i>Food Menu</a></li>
                    </ul>
                </li>

                <li class="treeview gallerycategory gallery">
                    <a href="#">
                        <i class="fa fa-list"></i>
                        <span>Gallery</span>
                    </a>
                    <ul class="treeview-menu">
                        <li class="gallery" ><a href="<?=$this->CreateUrl('/staff/Gallery/admin')?>"><i class="fa fa-circle-o"></i>Gallery</a></li>
                        <li class="gallerycategory" ><a href="<?=$this->CreateUrl('/staff/GalleryCategory/admin')?>"><i class="fa fa-circle-o"></i>Categories</a></li>
                    </ul>
                </li>

                <li class="treeview collective">
                    <a href="#">
                        <i class="fa fa-list"></i>
                        <span>Collective</span>
                    </a>
                    <ul class="treeview-menu">
                        <li class="collective" ><a href="<?=$this->CreateUrl('/staff/collective/admin')?>"><i class="fa fa-circle-o"></i>Collective</a></li>
                        <li class="collectiveexperience" ><a href="<?=$this->CreateUrl('/staff/collectiveExperience/admin')?>"><i class="fa fa-circle-o"></i>Work experience</a></li>
                    </ul>
                </li>

                <li class="treeview slider">
                    <a href="#">
                        <i class="fa fa-list"></i>
                        <span>Slider</span>
                    </a>
                    <ul class="treeview-menu">
                        <li class="slider" ><a href="<?=$this->CreateUrl('/staff/slider/admin')?>"><i class="fa fa-circle-o"></i>Slider</a></li>
                    </ul>
                </li>

                <li class="treeview translation">
                    <a href="<?=$this->CreateUrl('/staff/translation/admin')?>">
                        <i class="fa fa-file-word-o" aria-hidden="true"></i>
                        <span>Translation</span>
                    </a>
                </li>
                <li class="treeview followus contactus aboutus restauranttypes markers openingkeys">
                    <a href="#">
                        <i class="fa fa-list"></i>
                        <span>Information</span>
                    </a>
                    <ul class="treeview-menu">
                        <li class="treeview aboutus">
                            <a href="<?=$this->CreateUrl('/staff/AboutUs/update/id/1')?>">
                                <i class="fa fa-circle-o"></i>
                                <span>About Us</span>
                            </a>
                        </li>
                        <li class="treeview followus">
                            <a href="<?=$this->CreateUrl('/staff/FollowUs/admin')?>">
                                <i class="fa fa-circle-o"></i>
                                <span>Follow Us</span>
                            </a>
                        </li>
                        <li class="treeview contactus">
                            <a href="<?=$this->CreateUrl('/staff/ContactUs/admin')?>">
                                <i class="fa fa-phone"></i>
                                <span>Our Contacts</span>
                            </a>
                        </li>

                        <li class="treeview markers">
                            <a href="<?=$this->CreateUrl('/staff/Markers/admin')?>">
                                <i class="fa fa-circle-o"></i>
                                <span>Markers</span>
                            </a>
                        </li>

                        <li class="treeview aboutuspics">
                            <a href="<?=$this->CreateUrl('/staff/AboutUsPics/admin')?>">
                                <i class="fa fa-circle-o"></i>
                                <span>About Us Pics</span>
                            </a>
                        </li>

                        <li class="treeview restauranttypes">
                            <a href="<?=$this->CreateUrl('/staff/RestaurantTypes/admin')?>">
                                <i class="fa fa-circle-o"></i>
                                <span>Restaurant Types</span>
                            </a>
                        </li>
                        <li class="treeview openingkeys">
                            <a href="<?=$this->CreateUrl('/staff/OpeningKeys/admin')?>">
                                <i class="fa fa-circle-o"></i>
                                <span>Opening Keys</span>
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="treeview subscribe dontsenduser">
                    <a href="#">
                        <i class="fa fa-list"></i>
                        <span>Subscribes</span>
                    </a>
                    <ul class="treeview-menu">
                        <li class="subscribe" ><a href="<?=$this->CreateUrl('/staff/subscribe/admin')?>"><i class="fa fa-circle-o"></i>Subscribe People</a></li>
                        <li class="subscribe" ><a href="<?=$this->CreateUrl('/staff/subscribe/sendEmail')?>"><i class="fa fa-circle-o"></i>Send Email</a></li>
                        <li class="dontsenduser" ><a href="<?=$this->CreateUrl('/staff/dontSendUser/admin')?>"><i class="fa fa-circle-o"></i>Doesn't sent to</a></li>
                    </ul>
                </li>

                <li class="treeview events">
                    <a href="<?=$this->CreateUrl('/staff/events/admin')?>">
                        <i class="fa fa-bell" aria-hidden="true"></i>
                        <span>Events</span>
                    </a>
                </li>


                <li class="treeview icons">
                    <a href="<?=$this->CreateUrl('/staff/icons/index')?>">
                        <i class="fa fa-bell" aria-hidden="true"></i>
                        <span>Icons</span>
                    </a>
                </li>

                <li class="treeview menu">
                    <a href="#">
                        <i class="fa fa-list"></i>
                        <span>Menu</span>
                    </a>
                    <ul class="treeview-menu">
                        <li class="menu" ><a href="<?=$this->CreateUrl('/staff/Menu/admin')?>"><i class="fa fa-circle-o"></i>Header Menu</a></li>
                    </ul>
                </li>



            </ul>

        </section>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <?=$content?>
    </div>

    <footer class="footer">
        <div class="powered">
            <a href="http://voodoo.pro/" target="_blank">
                Powered by VooDoo programming
                <div class="voodoo">
                    <img class="img-responsive" src="<?=Yii::app()->request->baseUrl?>/vendor/assets/images/VooDooprogramming.png">
                </div>
            </a>

        </div>
    </footer>

    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
        <!-- Create the tabs -->

        <!-- Tab panes -->
        <div class="tab-content">
            <!-- Home tab content -->
            <div class="tab-pane" id="control-sidebar-home-tab">

                <!-- /.control-sidebar-menu -->

            </div>
            <!-- /.tab-pane -->
            <!-- Stats tab content -->
            <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div>
            <!-- /.tab-pane -->
            <!-- Settings tab content -->

            <!-- /.tab-pane -->
        </div>
    </aside>
    <!-- /.control-sidebar -->
    <!-- Add the sidebar's background. This div must be placed
         immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
<script>
    $('.<?=strtolower(Yii::app()->controller->id)?>').addClass('active')
</script>
<script>
    $.widget.bridge('uibutton', $.ui.button);
    $(function() {

        $('nav a[href^="/' + location.pathname.split("/")[1] + '"]').addClass('active');
    });
</script>
<!-- Bootstrap 3.3.5 -->
<script src="<?=Yii::app()->request->BaseUrl?>/vendor/assets/bootstrap/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<div id="line-chart">
</div>
<!-- Sparkline -->
<script src="<?=Yii::app()->request->BaseUrl?>/vendor/assets/sparkline/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="<?=Yii::app()->request->BaseUrl?>/vendor/assets/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="<?=Yii::app()->request->BaseUrl?>/vendor/assets/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="<?=Yii::app()->request->BaseUrl?>/vendor/assets/knob/jquery.knob.js"></script>


<!-- Bootstrap WYSIHTML5 -->
<script src="<?=Yii::app()->request->BaseUrl?>/vendor/assets/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="<?=Yii::app()->request->BaseUrl?>/vendor/assets/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?=Yii::app()->request->BaseUrl?>/vendor/assets/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?=Yii::app()->request->BaseUrl?>/vendor/assets/dist/js/app.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?=Yii::app()->request->BaseUrl?>/vendor/assets/dist/js/demo.js"></script>
</body>
</html>



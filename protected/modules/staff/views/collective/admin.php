
<section class="content-header">
	<h1>
		Collective
	</h1>

</section>
<section class="content">
	<div class="row">
		<div class="col-xs-12">
			<div class="box">
				<div class="box-header">
					<h3 class="box-title">Collective</h3>
					<a href="<?=$this->CreateUrl('Collective/create')?>" class="btn btn-primary pull-right">Add New</a>

				</div>
				<!-- /.box-header -->
				<div class="box-body">

						<?php $this->widget('zii.widgets.grid.CGridView', array(
							'id'=>'collective-grid',
							'dataProvider'=>$model->search(),
							'filter'=>$model,
							'columns'=>array(
								'id',
								array(
									'name'=>'image',
									'type' => 'raw',
									'htmlOptions'=>array(
										'class' => 'admin_img about_us_slider'
									),
									'value'=>'CHtml::image(Yii::app()->baseUrl . "/uploads/collective/" . $data->image)'
								),
								array(
									'name' 	=> 'first_name',
									'value'	=> '$data->collectiveLabel->first_name'
								),
								array(
									'class' => 'zii.widgets.grid.CButtonColumn',
									'htmlOptions' => array('style' => 'white-space: nowrap'),
									'afterDelete' => 'function(link,success,data) { if (success && data) alert(data); }',
									'buttons' => array(
										'view' => array(
											'options' => array('rel' => 'tooltip', 'data-toggle' => 'tooltip', 'title' => Yii::t('app', 'View'),'class'=>'img-btn'),
											'label' => '<i class="fa fa-eye"></i>',
											'imageUrl' => false,
										),
										'update' => array(
											'options' => array('rel' => 'tooltip', 'data-toggle' => 'tooltip', 'title' => Yii::t('app', 'Update'),'class'=>'img-btn'),
											'label' => '<i class="fa fa-pencil"></i>',
											'imageUrl' => false,
										),
										'delete' => array(
											'options' => array('rel' => 'tooltip', 'data-toggle' => 'tooltip', 'title' => Yii::t('app', 'Delete'),'class'=>'img-btn del-btn'),
											'label' => '<i class="fa fa-times"></i>',
											'imageUrl' => false,
										)
									)
								),
							),
						)); ?>
				</div>
			</div>
		</div>
	</div>
</section>
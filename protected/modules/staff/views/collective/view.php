<section class="content-header">
	<h1>
		Collective
		<small>#<?php echo $model->id; ?></small>
	</h1>

</section>
<section class="content">
	<div class="row">
		<div class="col-xs-12">
			<div class="box">
				<div class="box-header">
					<h3 class="box-title">Collective #<?php echo $model->id; ?></h3>
					<a href="<?=$this->CreateUrl('Collective/update/id/'.$model->id)?>" class="btn btn-success pull-right" style="margin-left: 15px">Edit</a>
					<a href="<?=$this->CreateUrl('Collective/create')?>" class="btn btn-primary pull-right">Add New</a>
				</div>
				<!-- /.box-header -->
				<div class="box-body">

						<?php $this->widget('zii.widgets.CDetailView', array(
							'data'=>$model,
							'htmlOptions' => array('class' => 'table table-bordered table-hover'),
							'attributes'=>array(
								'id',
								'sort_order',
								array(
									'name' 	=> 'first_name',
									'value'	=> $model->collectiveLabel->first_name,
									'type'	=> 'raw'
								),
								array(
									'name' 	=> 'last_name',
									'value'	=> $model->collectiveLabel->last_name,
									'type'	=> 'raw'
								),
								array(
									'name' 	=> 'position',
									'value'	=> $model->collectiveLabel->position,
									'type'	=> 'raw'
								),
								array(
									'name' 	=> 'description',
									'value'	=> $model->collectiveLabel->description,
									'type'	=> 'raw'
								),

								array(
									'name'=>'image',
									'type' => 'raw',
									'htmlOptions'=>array(
										'class' => 'admin_img about_us_slider'
									),
									'value'=>CHtml::image(Yii::app()->baseUrl . "/uploads/collective/" . $model->image)
								),
							),
						)); ?>
				</div>
			</div>
		</div>
	</div>
</section>
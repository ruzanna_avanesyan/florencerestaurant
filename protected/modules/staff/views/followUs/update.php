<?php
/* @var $this FollowUsController */
/* @var $model FollowUs */

$this->breadcrumbs=array(
	'Follow Uses'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List FollowUs', 'url'=>array('index')),
	array('label'=>'Create FollowUs', 'url'=>array('create')),
	array('label'=>'View FollowUs', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage FollowUs', 'url'=>array('admin')),
);
?>

<h1>Update FollowUs <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>
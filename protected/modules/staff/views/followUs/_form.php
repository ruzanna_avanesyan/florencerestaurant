<!-- Main content -->
<section class="content">
	<div class="row">
		<!-- left column -->
		<div class="col-md-12">
			<!-- general form elements -->
			<div class="box box-primary">
				<div class="box-header with-border">
					<h3 class="box-title"><p class="note">Fields with <span class="required">*</span> are required.</p></h3>
				</div>

					<?php $form=$this->beginWidget('CActiveForm', array(
						'id'=>'follow-us-form',
						'enableAjaxValidation'=>false,
						'htmlOptions' => array(
							'class' =>'form-horizontal'
						)
					)); ?>

						<?php echo $form->errorSummary($model); ?>

						<div class="form-group">
							<?php echo $form->labelEx($model,'url',array('class'=>'col-sm-2 control-label')); ?>
							<div class="col-sm-10">
								<?php echo $form->textField($model,'url',array('size'=>60,'maxlength'=>250, 'class' => 'form-control')); ?>
							</div>
							<?php echo $form->error($model,'url'); ?>
						</div>

						<div class="form-group">
							<?php echo $form->labelEx($model,'sort_order',array('class'=>'col-sm-2 control-label')); ?>
							<div class="col-sm-10">
								<?php echo $form->textField($model,'sort_order', array('class' => 'form-control')); ?>
							</div>
							<?php echo $form->error($model,'sort_order'); ?>
						</div>

						<div class="form-group">
							<?php echo $form->labelEx($model,'fa_icon',array('class'=>'col-sm-2 control-label')); ?>
							<div class="col-sm-10">
								<?php echo $form->textField($model,'fa_icon',array('size'=>60,'maxlength'=>250,'class' => 'form-control')); ?>
							</div>
							<?php echo $form->error($model,'fa_icon'); ?>
						</div>

						<div class="form-group buttons">
							<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',array('class'=>'btn btn-primary pull-right')); ?>
						</div>

					<?php $this->endWidget(); ?>

			</div>
		</div>
	</div>
</section>
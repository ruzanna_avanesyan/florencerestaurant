<?php
/* @var $this FollowUsController */
/* @var $model FollowUs */

$this->breadcrumbs=array(
	'Follow Uses'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List FollowUs', 'url'=>array('index')),
	array('label'=>'Manage FollowUs', 'url'=>array('admin')),
);
?>

<h1>Create FollowUs</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>
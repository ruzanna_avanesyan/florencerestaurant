<?php
/* @var $this FollowUsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Follow Uses',
);

$this->menu=array(
	array('label'=>'Create FollowUs', 'url'=>array('create')),
	array('label'=>'Manage FollowUs', 'url'=>array('admin')),
);
?>

<h1>Follow Uses</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>

<section class="content-header">
	<h1>
		Contacts
		<small>#<?php echo $model->id; ?></small>
	</h1>

</section>
<section class="content">
	<div class="row">
		<div class="col-xs-12">
			<div class="box">
				<div class="box-header">
					<h3 class="box-title">Contacts #<?php echo $model->id; ?></h3>
					<a href="<?=$this->CreateUrl('ContactUs/update/id/'.$model->id)?>" class="btn btn-success pull-right" style="margin-left: 15px">Edit</a>

				</div>
				<!-- /.box-header -->
				<div class="box-body">

						<?php $this->widget('zii.widgets.CDetailView', array(
							'data'=>$model,
							'htmlOptions' => array('class' => 'table table-bordered table-hover'),
							'attributes'=>array(
								'id',
								'phone',
								'email',
								array(
									'name' 	=> 'address',
									'value'	=> $model->contactUsLabel->address,
									'type'	=> 'raw'
								)
							),
						)); ?>
				</div>
			</div>
		</div>
	</div>
</section>

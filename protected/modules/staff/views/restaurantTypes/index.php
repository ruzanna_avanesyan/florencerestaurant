<?php
/* @var $this RestaurantTypesController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Restaurant Types',
);

$this->menu=array(
	array('label'=>'Create RestaurantTypes', 'url'=>array('create')),
	array('label'=>'Manage RestaurantTypes', 'url'=>array('admin')),
);
?>

<h1>Restaurant Types</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>

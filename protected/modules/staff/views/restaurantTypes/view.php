<section class="content-header">
	<h1>
		Restaurant Types
		<small>#<?php echo $model->id; ?></small>
	</h1>

</section>
<section class="content">
	<div class="row">
		<div class="col-xs-12">
			<div class="box">
				<div class="box-header">
					<h3 class="box-title">Restaurant Types#<?php echo $model->id; ?></h3>
					<a href="<?=$this->CreateUrl('RestaurantTypes/update/id/'.$model->id)?>" class="btn btn-success pull-right" style="margin-left: 15px">Edit</a>
				</div>
				<!-- /.box-header -->
				<div class="box-body">

						<?php $this->widget('zii.widgets.CDetailView', array(
							'data'=>$model,
							'htmlOptions' => array('class' => 'table table-bordered table-hover'),
							'attributes'=>array(
								'id',
								array(
									'name' 	=> 'name',
									'value'	=> $model->restaurantTypesLabel->name
								),
								array(
									'name' 	=> 'small_description',
									'value'	=> $model->restaurantTypesLabel->small_description
								),
								array(
									'name' 	=> 'description',
									'value'	=> $model->restaurantTypesLabel->description,
									'type'	=> 'raw'
								),
								'sort_order',
								array(
									'name'=>'image',
									'type' => 'raw',
									'htmlOptions'=>array(
										'class' => 'admin_img about_us_slider'
									),
									'value'=>CHtml::image(Yii::app()->baseUrl . "/uploads/collective/" . $model->image)
								),
							),
						)); ?>
				</div>
			</div>
		</div>
	</div>
</section>
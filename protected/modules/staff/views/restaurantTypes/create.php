<?php
/* @var $this RestaurantTypesController */
/* @var $model RestaurantTypes */

$this->breadcrumbs=array(
	'Restaurant Types'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List RestaurantTypes', 'url'=>array('index')),
	array('label'=>'Manage RestaurantTypes', 'url'=>array('admin')),
);
?>

<h1>Create RestaurantTypes</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>
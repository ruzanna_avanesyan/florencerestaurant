<?php
/* @var $this RestaurantTypesController */
/* @var $model RestaurantTypes */

$this->breadcrumbs=array(
	'Restaurant Types'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List RestaurantTypes', 'url'=>array('index')),
	array('label'=>'Create RestaurantTypes', 'url'=>array('create')),
	array('label'=>'View RestaurantTypes', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage RestaurantTypes', 'url'=>array('admin')),
);
?>

<h1>Update RestaurantTypes <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>
<section class="content">
	<div class="row">
		<!-- left column -->
		<div class="col-md-12">
			<!-- general form elements -->
			<div class="box box-primary">
				<div class="box-header with-border">
					<h3 class="box-title"><p class="note">Fields with <span class="required">*</span> are required.</p></h3>
					<h4>Image size  - 405x405</h4>
				</div>
					<?php $form=$this->beginWidget('CActiveForm', array(
						'id'=>'food-menu-form',
						'enableAjaxValidation'=>false,
						'htmlOptions' => array(
							'class' =>'form-horizontal',
							"enctype" => "multipart/form-data"
						)
					)); ?>

						<?php echo $form->errorSummary($model); ?>

						<div class="form-group">
							<?php echo $form->labelEx($model,'image* (405x405)',array('class'=>'col-sm-2 control-label')); ?>
							<div class="col-sm-10">
								<?php echo $form->fileField($model,'image',array('size'=>60,'maxlength'=>250,'class' => 'form-control')); ?>
							</div>
							<?php echo $form->error($model,'image'); ?>
						</div>

						<div class="form-group">
							<?php echo $form->labelEx($model,'sort_order',array('class'=>'col-sm-2 control-label')); ?>
							<div class="col-sm-10">
								<?php echo $form->textField($model,'sort_order', array('class' => 'form-control')); ?>
							</div>
							<?php echo $form->error($model,'sort_order'); ?>
						</div>
				<div id="form-group">
					<label class="col-sm-2 control-label">Content</label>
					<div class="col-sm-10">
						<ul id="tabs" class="nav nav-tabs" data-tabs="tabs">
							<?php
							$languages = Helpers::languages();
							if(!empty($languages)):
								foreach($languages as $key => $value):
									$active = "";
									if($key == 0){
										$active = "active";
									}
									?>
									<li class="<?=$active?>"><a href="#<?=$value->id?>" data-toggle="tab" ><?=$value->name?></a></li>
									<?php
								endforeach;
							endif;
							?>
						</ul>
						<div id="my-tab-content" class="tab-content">
							<?php
							$languages = Helpers::languages();
							if(!empty($languages)):
								foreach($languages as $key => $value):
									$active = "";
									if($key == 0){
										$active = "active";
									}
									$content = FoodMenuLabel::model()->findByAttributes(array('language_id' => $value->id,'menu_id' => $model->id));
									$name = "";
									$description = "";
									if(!empty($content)){
										$name = $content->name;
										$description = $content->description;
									}
									?>
									<div class="tab-pane <?=$active?>" id="<?=$value->id?>" >
										<div class="form-group">
											<label class="col-sm-1 control-label">Name</label>
											<div class="col-sm-11">
												<input type="text" class="form-control" name = "FoodMenuLabel[name][<?=$value->id?>]" value="<?=$name?>" required />
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-1 control-label">Description</label>
											<div class="col-sm-11">
												<textarea class="form-control"  id="editor<?=$value->id?>" name = "FoodMenuLabel[description][<?=$value->id?>]" required><?=$description?></textarea>
											</div>
										</div>
									</div>

									<?php
								endforeach;
							endif;
							?>
						</div>
					</div>
				</div>

						<div class="form-group buttons">
							<div class="col-sm-12">
								<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',array('class'=>'btn btn-primary pull-right')); ?>
							</div>
						</div>

					<?php $this->endWidget(); ?>
			</div>
		</div>
	</div>
</section>


<script>
	$(function () {
		// Replace the <textarea id="editor1"> with a CKEditor
		// instance, using default configuration.
		<?php
		$language = Helpers::languages();
		if(!empty($language)):
		foreach($language as $key => $value):
		?>
		CKEDITOR.replace( 'editor<?=$value->id?>', {});
		//bootstrap WYSIHTML5 - text editor
		$(".textarea").wysihtml5();
		<?php
		endforeach;
		endif;
		?>



	});
</script>

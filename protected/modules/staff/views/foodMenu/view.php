<section class="content-header">
	<h1>
		Food Menu
		<small>#<?php echo $model->id; ?></small>
	</h1>

</section>
<section class="content">
	<div class="row">
		<div class="col-xs-12">
			<div class="box">
				<div class="box-header">
					<h3 class="box-title">Food Menu #<?php echo $model->id; ?></h3>
					<a href="<?=$this->CreateUrl('foodMenu/update/id/'.$model->id)?>" class="btn btn-success pull-right" style="margin-left: 15px">Edit</a>
					<a href="<?=$this->CreateUrl('foodMenu/create')?>" class="btn btn-primary pull-right">Add New</a>
				</div>
				<!-- /.box-header -->
				<div class="box-body">

					<?php $this->widget('zii.widgets.CDetailView', array(
						'data'=>$model,
						'htmlOptions' => array('class' => 'table table-bordered table-hover'),
						'attributes'=>array(
							'id',
							'sort_order',
							array(
								'name' 	=> 'Name',
								'value'	=> Helpers::Name($model,'foodMenuLabels','name'),
								'type'	=> 'raw'
							),

							array(
								'name' 	=> 'Description',
								'value'	=> Helpers::Name($model,'foodMenuLabels','description'),
								'type'	=> 'raw'
							),
							array(
								'name'=>'image',
								'type' => 'raw',
								'htmlOptions'=>array(
									'class' => 'admin_img about_us_slider'
								),
								'value'=>CHtml::image(Yii::app()->baseUrl . "/uploads/menu/" . $model->image)


							),
						),
					)); ?>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="content-header">
	<h1>
		Food Menu
	</h1>

</section>
<section class="content">
	<div class="row">
		<div class="col-xs-12">
			<div class="box">
				<div class="box-header">
					<h3 class="box-title">Food Menu</h3>
					<a href="<?=$this->CreateUrl('FoodMenu/create')?>" class="btn btn-primary pull-right">Add New</a>
				</div>
				<!-- /.box-header -->
				<div class="box-body">

						<?php $this->widget('zii.widgets.grid.CGridView', array(
							'id'=>'food-menu-grid',
							'dataProvider'=>$model->search(),
							'filter'=>$model,
							'columns'=>array(
								'id',
								array(
									'name'=>'image',
									'type' => 'raw',
									'htmlOptions'=>array(
										'class' => 'admin_img food_menu'
									),
									'value'=>'CHtml::image(Yii::app()->baseUrl . "/uploads/menu/" . $data->image)'


								),
								array(
									'name' 	=> 'name',
									'value'	=> '$data->foodMenuLabel->name'
								),
								'sort_order',
								array(
									'class' => 'zii.widgets.grid.CButtonColumn',
									'htmlOptions' => array('style' => 'white-space: nowrap'),
									'afterDelete' => 'function(link,success,data) { if (success && data) alert(data); }',
									'buttons' => array(
										'view' => array(
											'options' => array('rel' => 'tooltip', 'data-toggle' => 'tooltip', 'title' => Yii::t('app', 'View'),'class'=>'img-btn'),
											'label' => '<i class="fa fa-eye"></i>',
											'imageUrl' => false,
										),
										'update' => array(
											'options' => array('rel' => 'tooltip', 'data-toggle' => 'tooltip', 'title' => Yii::t('app', 'Update'),'class'=>'img-btn'),
											'label' => '<i class="fa fa-pencil"></i>',
											'imageUrl' => false,
										),
										'delete' => array(
											'options' => array('rel' => 'tooltip', 'data-toggle' => 'tooltip', 'title' => Yii::t('app', 'Delete'),'class'=>'img-btn del-btn'),
											'label' => '<i class="fa fa-times"></i>',
											'imageUrl' => false,
										)
									)
								),
							),
						)); ?>
				</div>
			</div>
		</div>
	</div>
</section>
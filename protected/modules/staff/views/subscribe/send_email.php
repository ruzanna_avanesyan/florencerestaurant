<section class="content">
    <div class="row">
        <!-- left column -->
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title"><p class="note">Fields with <span class="required">*</span> are required.</p></h3>
                </div>
                        <form class="form-horizontal" action="<?=$this->CreateUrl('subscribe/SendEmail')?>" method="post">
                            <div class="form-group">
                                <div class="col-sm-2 control-label">
                                    Subject
                                </div>
                                <div class="col-sm-10">
                                    <input type="text" name="send[subject]" class="form-control" required />
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-2 control-label">
                                    Message
                                </div>
                                <div class="col-sm-10">
                                    <textarea class="form-control" id="editor" name="send[message]" required></textarea>
                                </div>
                            </div>
                            <div class="form-group buttons">
                                <div class="col-sm-12">
                                    <button class="btn btn-primary pull-right">Send</button>
                                </div>
                            </div>
                        </form>

            </div>
        </div>
    </div>
</section>
<script>
    $(function () {
        // Replace the <textarea id="editor1"> with a CKEditor
        // instance, using default configuration.

        CKEDITOR.replace( 'editor', {});
        //bootstrap WYSIHTML5 - text editor
        $(".textarea").wysihtml5();



    });
</script>
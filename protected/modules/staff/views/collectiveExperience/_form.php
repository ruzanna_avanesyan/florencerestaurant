<!-- Main content -->
<section class="content">
	<div class="row">
		<!-- left column -->
		<div class="col-md-12">
			<!-- general form elements -->
			<div class="box box-primary">
				<div class="box-header with-border">
					<h3 class="box-title"><p class="note">Fields with <span class="required">*</span> are required.</p></h3>
				</div>

					<?php $form=$this->beginWidget('CActiveForm', array(
						'id'=>'collective-experience-form',
						'enableAjaxValidation'=>false,
						'htmlOptions' => array(
							'class' =>'form-horizontal',
							"enctype" => "multipart/form-data"
						)
					)); ?>


						<?php echo $form->errorSummary($model); ?>

						<div class="form-group">
							<?php echo $form->labelEx($model,'collective_id',array('class'=>'col-sm-2 control-label')); ?>
							<div class="col-sm-10">
								<select class="form-control" name="CollectiveExperience[collective_id]">
									<?php
										if(!empty($collective)):
											foreach($collective as $value):
												$s = "";
												if($value->id == $model->collective_id){
													$s = "selected";
												}
									?>
												<option <?=$s?> value="<?=$value->id?>"><?=$value->collectiveLabel->first_name.' '.$value->collectiveLabel->last_name?></option>
									<?php
											endforeach;
										endif;
									?>
								</select>
							</div>
							<?php echo $form->error($model,'collective_id'); ?>
						</div>

						<div class="form-group">
							<?php echo $form->labelEx($model,'image* (162x107)',array('class'=>'col-sm-2 control-label')); ?>
							<div class="col-sm-10">
								<?php echo $form->fileField($model,'image',array('size'=>60,'maxlength'=>250,'class' => 'form-control')); ?>
							</div>
							<?php echo $form->error($model,'image'); ?>
						</div>

						<div class="form-group buttons">
							<div class="col-sm-12">
								<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',array('class'=>'btn btn-primary pull-right')); ?>
							</div>
						</div>

					<?php $this->endWidget(); ?>
			</div>
		</div>
	</div>
</section>
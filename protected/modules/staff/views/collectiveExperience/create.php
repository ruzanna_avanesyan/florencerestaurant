<?php
/* @var $this CollectiveExperienceController */
/* @var $model CollectiveExperience */

$this->breadcrumbs=array(
	'Collective Experiences'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List CollectiveExperience', 'url'=>array('index')),
	array('label'=>'Manage CollectiveExperience', 'url'=>array('admin')),
);
?>

<h1>Create CollectiveExperience</h1>

<?php $this->renderPartial('_form', array('model'=>$model,'collective'	=> $collective)); ?>
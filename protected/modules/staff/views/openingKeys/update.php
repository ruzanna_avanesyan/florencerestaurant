<?php
/* @var $this OpeningKeysController */
/* @var $model OpeningKeys */

$this->breadcrumbs=array(
	'Opening Keys'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List OpeningKeys', 'url'=>array('index')),
	array('label'=>'Create OpeningKeys', 'url'=>array('create')),
	array('label'=>'View OpeningKeys', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage OpeningKeys', 'url'=>array('admin')),
);
?>

<h1>Update OpeningKeys <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>
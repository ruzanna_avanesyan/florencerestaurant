<?php
/* @var $this OpeningKeysController */
/* @var $model OpeningKeys */

$this->breadcrumbs=array(
	'Opening Keys'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List OpeningKeys', 'url'=>array('index')),
	array('label'=>'Manage OpeningKeys', 'url'=>array('admin')),
);
?>

<h1>Create OpeningKeys</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>
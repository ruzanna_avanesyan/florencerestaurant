<section class="content-header">
	<h1>
		Opening Keys
		<small>#<?php echo $model->id; ?></small>
	</h1>

</section>
<section class="content">
	<div class="row">
		<div class="col-xs-12">
			<div class="box">
				<div class="box-header">
					<h3 class="box-title">Opening Keys #<?php echo $model->id; ?></h3>
					<a href="<?=$this->CreateUrl('OpeningKeys/update/id/'.$model->id)?>" class="btn btn-success pull-right" style="margin-left: 15px">Edit</a>
					<a href="<?=$this->CreateUrl('OpeningKeys/create')?>" class="btn btn-primary pull-right">Add New</a>
				</div>
				<!-- /.box-header -->
				<div class="box-body">

						<?php $this->widget('zii.widgets.CDetailView', array(
							'data'=>$model,
							'attributes'=>array(
								'id',
								'sort_order',
								'time',
								array(
									'name' 	=> 'name',
									'value'	=> Helpers::Name($model,'openingKeysLabels','name'),
									'type'  => 'raw'
								),
							),
						)); ?>
				</div>
			</div>
		</div>
	</div>
</section>
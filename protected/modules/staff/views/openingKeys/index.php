<?php
/* @var $this OpeningKeysController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Opening Keys',
);

$this->menu=array(
	array('label'=>'Create OpeningKeys', 'url'=>array('create')),
	array('label'=>'Manage OpeningKeys', 'url'=>array('admin')),
);
?>

<h1>Opening Keys</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>

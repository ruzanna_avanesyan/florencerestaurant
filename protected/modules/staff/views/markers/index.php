<?php
/* @var $this MarkersController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Markers',
);

$this->menu=array(
	array('label'=>'Create Markers', 'url'=>array('create')),
	array('label'=>'Manage Markers', 'url'=>array('admin')),
);
?>

<h1>Markers</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>

<!-- Main content -->
<section class="content">
	<div class="row">
		<!-- left column -->
		<div class="col-md-12">
			<!-- general form elements -->
			<div class="box box-primary">
				<div class="box-header with-border">
					<h3 class="box-title"><p class="note">Fields with <span class="required">*</span> are required.</p></h3>
				</div>

						<?php $form=$this->beginWidget('CActiveForm', array(
							'id'=>'markers-form',
							'enableAjaxValidation'=>false,
							'htmlOptions' => array(
								'class' =>'form-horizontal'
							)
						)); ?>

						

							<?php echo $form->errorSummary($model); ?>

							<div class="form-group">
								<?php echo $form->labelEx($model,'lat',array('class'=>'col-sm-2 control-label')); ?>
								<div class="col-sm-10">
									<?php echo $form->textField($model,'lat',array('size'=>60,'maxlength'=>250,'class' => 'form-control','id' => 'lat')); ?>
								</div>
								<?php echo $form->error($model,'lat'); ?>
							</div>

							<div class="form-group">
								<?php echo $form->labelEx($model,'lng',array('class'=>'col-sm-2 control-label')); ?>
								<div class="col-sm-10">
									<?php echo $form->textField($model,'lng',array('size'=>60,'maxlength'=>250,'class' => 'form-control', 'id' => 'lng')); ?>
								</div>
								<?php echo $form->error($model,'lng'); ?>
							</div>

							<div id="map" style="width: 100%; height: 500px"></div>
							<div class="form-group buttons">
								<div class="col-sm-12">
									<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',array('class'=>'btn btn-primary pull-right')); ?>
								</div>
							</div>

						<?php $this->endWidget(); ?>
			</div>
		</div>
	</div>
</section>


<script type="text/javascript"
		src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBuU_0_uLMnFM-2oWod_fzC0atPZj7dHlU&callback=initMap" async defer></script>

<script>
	$(document).ready(function() {
		$(window).resize(function() {
			google.maps.event.trigger(map, 'resize');
		});
		google.maps.event.trigger(map, 'resize');
	});
	var map;
	var url = '<?=Yii::app()->request->BaseUrl?>';

	function initMap() {
		var myLatLng = {lat: 40.14948820651523, lng: 44.5111083984375};

		map = new google.maps.Map(document.getElementById('map'), {
			zoom: 10,
			center: myLatLng
		});
		var marker = new google.maps.Marker({
//			position: myLatLng,
			map: map,
			title: 'Hello World!'
		});

		google.maps.event.addListener(map, 'click', function(event) {
			placeMarker(event.latLng,map);
		});

	}
	var marker;
	function placeMarker(location,map) {
		if ( marker ) {
			marker.setPosition(location);
		} else {
			marker = new google.maps.Marker({
				position: location,
				map: map
			});
		}
		var lat = marker.getPosition().lat();
		var lng = marker.getPosition().lng();
		$('#lat').val(lat);
		$('#lng').val(lng);

	}
//
//
//		function initialize() {
//		var mapOptions = {
//			zoom: 16,
//			scrollwheel: false,
//			center: new google.maps.LatLng(40.1946667, 44.482081)
//		};
//		var map = new google.maps.Map(document.getElementById('googleMap'),
//			mapOptions);
//		var marker = new google.maps.Marker({
//			position: map.getCenter(),
//			animation: google.maps.Animation.BOUNCE,
//			icon: '<?//=Yii::app()->request->BaseUrl?>///vendor/img/map-marker.png',
//			map: map
//		});
//			google.maps.event.addListener(map, 'click', function(event) {
//				placeMarker(event.latLng);
//			});
//
//			function placeMarker(location) {
//				var marker = new google.maps.Marker({
//					position: location,
//					map: map
//				});
//			}
//	}
//	google.maps.event.addDomListener(window, 'load', initialize);
</script>
<!-- google map  js -->
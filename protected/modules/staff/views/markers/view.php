<section class="content-header">
	<h1>
		Markers
		<small>#<?php echo $model->id; ?></small>
	</h1>

</section>
<section class="content">
	<div class="row">
		<div class="col-xs-12">
			<div class="box">
				<div class="box-header">
					<h3 class="box-title">Markers #<?php echo $model->id; ?></h3>
					<a href="<?=$this->CreateUrl('Markers/update/id/'.$model->id)?>" class="btn btn-success pull-right" style="margin-left: 15px">Edit</a>
				</div>
				<!-- /.box-header -->
				<div class="box-body">

					<?php $this->widget('zii.widgets.CDetailView', array(
						'data'=>$model,
						'htmlOptions' => array('class' => 'table table-bordered table-hover'),
						'attributes'=>array(
							'id',
							'lat',
							'lng',
						),
					)); ?>
					<div id="map" style="width: 100%; height: 500px"></div>
				</div>
			</div>
		</div>
	</div>
</section>


<script type="text/javascript"
		src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBuU_0_uLMnFM-2oWod_fzC0atPZj7dHlU&callback=initMap" async defer></script>


<script type="text/javascript">


	var map;
	var url = '<?=Yii::app()->request->BaseUrl?>';

	function initMap() {

		var myLatLng = {lat: <?=$model->lat?>, lng: <?=$model->lng?>};

		map = new google.maps.Map(document.getElementById('map'), {
			zoom: 10,
			center: myLatLng
		});
		var marker = new google.maps.Marker({
			position: myLatLng,
			map: map,
			title: 'Hello World!'
		});


	}



</script>
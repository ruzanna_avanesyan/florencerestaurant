<?php
/* @var $this AboutUsPicsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'About Us Pics',
);

$this->menu=array(
	array('label'=>'Create AboutUsPics', 'url'=>array('create')),
	array('label'=>'Manage AboutUsPics', 'url'=>array('admin')),
);
?>

<h1>About Us Pics</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>

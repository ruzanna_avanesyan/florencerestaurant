
<section class="content-header">
	<h1>
		About Us
	</h1>

</section>
<section class="content">
	<div class="row">
		<div class="col-xs-12">
			<div class="box">
				<div class="box-header">
					<h3 class="box-title">About Us</h3>
				</div>
				<!-- /.box-header -->
				<div class="box-body">

					<?php $this->widget('zii.widgets.grid.CGridView', array(
						'id'=>'about-us-pics-grid',
						'dataProvider'=>$model->search(),
						'filter'=>$model,
						'columns'=>array(
							'id',
							'name',
							array(
								'name'=>'image',
								'type' => 'raw',
								'htmlOptions'=>array(
									'class' => 'admin_img about_us_slider'
								),
								'value'=>'CHtml::image(Yii::app()->baseUrl . "/uploads/slider/" . $data->image)'


							),
							array(
								'class' => 'zii.widgets.grid.CButtonColumn',
								'htmlOptions' => array('style' => 'white-space: nowrap'),
								'afterDelete' => 'function(link,success,data) { if (success && data) alert(data); }',
								'buttons' => array(
									'view' => array(
										'options' => array('rel' => 'tooltip', 'data-toggle' => 'tooltip', 'title' => Yii::t('app', 'View'),'class'=>'img-btn hide hide'),
										'label' => '<i class="fa fa-eye"></i>',
										'imageUrl' => false,
									),
									'update' => array(
										'options' => array('rel' => 'tooltip', 'data-toggle' => 'tooltip', 'title' => Yii::t('app', 'Update'),'class'=>'img-btn'),
										'label' => '<i class="fa fa-pencil"></i>',
										'imageUrl' => false,
									),
									'delete' => array(
										'options' => array('rel' => 'tooltip', 'data-toggle' => 'tooltip', 'title' => Yii::t('app', 'Delete'),'class'=>'img-btn del-btn hide'),
										'label' => '<i class="fa fa-times"></i>',
										'imageUrl' => false,
									)
								)
							),
						),
					)); ?>
				</div>
			</div>
		</div>
	</div>
</section>
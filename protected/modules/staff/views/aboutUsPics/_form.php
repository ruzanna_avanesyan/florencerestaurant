<!-- Main content -->
<section class="content">
	<div class="row">
		<!-- left column -->
		<div class="col-md-12">
			<!-- general form elements -->
			<div class="box box-primary">
				<div class="box-header with-border">
					<h3 class="box-title"><p class="note">Fields with <span class="required">*</span> are required.</p></h3>
					<h3 class="box-title">Image size = 640x632</h3>
				</div>
						<?php $form=$this->beginWidget('CActiveForm', array(
							'id'=>'about-us-pics-form',
							'enableAjaxValidation'=>false,
							'htmlOptions' => array(
								'class' =>'form-horizontal',
								"enctype" => "multipart/form-data"
							)
						)); ?>

							<p class="note">Fields with <span class="required">*</span> are required.</p>

							<?php echo $form->errorSummary($model); ?>

							<div class="form-group">
								<?php echo $form->labelEx($model,'image',array('class'=>'col-sm-2 control-label')); ?>
								<div class="col-sm-10">
									<?php echo $form->fileField($model,'image',array('size'=>60,'maxlength'=>250,'class'=>'form-control')); ?>
								</div>
								<?php echo $form->error($model,'image'); ?>
							</div>

							<div class="form-group">
								<?php echo $form->labelEx($model,'name',array('class'=>'col-sm-2 control-label')); ?>
								<div class="col-sm-10">
									<?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>250,'class'=>'form-control','readonly' => 'readonly')); ?>
								</div>
								<?php echo $form->error($model,'name'); ?>
							</div>

							<div class="form-group buttons">
								<div class="col-sm-12">
									<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',array('class'=>'btn btn-primary pull-right')); ?>
								</div>
							</div>

						<?php $this->endWidget(); ?>
	</div>
	</div>
	</div>
</section>

<?php
/* @var $this AboutUsPicsController */
/* @var $model AboutUsPics */

$this->breadcrumbs=array(
	'About Us Pics'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List AboutUsPics', 'url'=>array('index')),
	array('label'=>'Create AboutUsPics', 'url'=>array('create')),
	array('label'=>'Update AboutUsPics', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete AboutUsPics', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage AboutUsPics', 'url'=>array('admin')),
);
?>

<h1>View AboutUsPics #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'image',
		'name',
	),
)); ?>

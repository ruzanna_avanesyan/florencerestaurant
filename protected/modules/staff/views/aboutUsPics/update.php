<?php
/* @var $this AboutUsPicsController */
/* @var $model AboutUsPics */

$this->breadcrumbs=array(
	'About Us Pics'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List AboutUsPics', 'url'=>array('index')),
	array('label'=>'Create AboutUsPics', 'url'=>array('create')),
	array('label'=>'View AboutUsPics', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage AboutUsPics', 'url'=>array('admin')),
);
?>

<h1>Update AboutUsPics <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>
<?php
/* @var $this AboutUsPicsController */
/* @var $model AboutUsPics */

$this->breadcrumbs=array(
	'About Us Pics'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List AboutUsPics', 'url'=>array('index')),
	array('label'=>'Manage AboutUsPics', 'url'=>array('admin')),
);
?>

<h1>Create AboutUsPics</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>
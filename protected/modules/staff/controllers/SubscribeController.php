<?php

class SubscribeController extends Controller
{
	public $layout = "/layouts/main";

	public $canWithoutLogin = array();
	function init() {
		$this->canWithoutLogin = array('login');

		if(!isset(Yii::app()->session['stuff'])) {
			if (!in_array($this->action, $this->canWithoutLogin)) {
				$this->redirect($this->CreateUrl('default/login'));
			}
		}
	}

	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Subscribe;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Subscribe']))
		{
			$model->attributes=$_POST['Subscribe'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Subscribe']))
		{
			$model->attributes=$_POST['Subscribe'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Subscribe');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Subscribe('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Subscribe']))
			$model->attributes=$_GET['Subscribe'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Subscribe the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Subscribe::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Subscribe $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='subscribe-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

	public function actionSendEmail(){
		if(isset($_POST['send'])) {
			$subject = $_POST['send']['subject'];
			$message = $_POST['send']['message'];
			set_time_limit(0);
			$limit = 50;
			$subscribe = Subscribe::model()->findAll();
			foreach ($subscribe as $key => $value) {

				if ($this->sendEmailForAllUser($value->email, $subject, $message)) {
				} else {
					$dont_send = new DontSendUser();
					$dont_send->user_id = $value->id;
					$dont_send->date = date('Y-m-d H:i:s');
					$dont_send->save();
				}
			}
			$this->redirect($this->CreateUrl('DontSendUser/admin'));

		}
		$this->render('send_email');
	}

	protected function sendEmailForAllUser($email,$subject, $message) {
		$mail = Yii::app()->params->adminEmail;
		$password = Yii::app()->params->password;
		$smtp = Yii::app()->params->smtp;
		$mailSMTP = new SendMailSmtpClass( $mail, $password, 'ssl://'.$smtp, $mail, 465);

		$headers= "MIME-Version: 1.0\r\n";
		$headers .= "Content-type: text/html; charset=utf-8\r\n";
		$headers .= "From:  <$mail>\r\n";
		$result =  $mailSMTP->send("$email",
			"

            : ".$subject,
			$message." <br/> <a href=".$this->createUrl("/site/unsubscribe",array('email'=>$email))." > Unsubscribe </a>",
			$headers);
		if($result === true){
			return true;
		}else {
			return false;
		}
	}
}

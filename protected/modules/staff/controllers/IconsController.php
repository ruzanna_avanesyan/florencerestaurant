<?php

/**
 * Created by PhpStorm.
 * User: voodoo
 * Date: 9/17/16
 * Time: 2:24 PM
 */
class IconsController extends Controller
{
    public $layout = "/layouts/main";

    public $canWithoutLogin = array();
    function init() {
        $this->canWithoutLogin = array('login');

        if(!isset(Yii::app()->session['stuff'])) {
            if (!in_array($this->action, $this->canWithoutLogin)) {
                $this->redirect($this->CreateUrl('default/login'));
            }
        }
    }

    public function actionIndex(){
        $this->render('index');
    }

}
<?php

class AboutUsPicsController extends Controller
{
	public $layout = "/layouts/main";

	public $canWithoutLogin = array();
	function init() {
		$this->canWithoutLogin = array('login');

		if(!isset(Yii::app()->session['stuff'])) {
			if (!in_array($this->action, $this->canWithoutLogin)) {
				$this->redirect($this->CreateUrl('default/login'));
			}
		}
	}


	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		die;
		$model=new AboutUsPics;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['AboutUsPics']))
		{
			$model->attributes=$_POST['AboutUsPics'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['AboutUsPics']))
		{
			$oldImage = $model->image;
			$model->attributes=$_POST['AboutUsPics'];
			$model->image = $oldImage;
			if(!empty($_FILES["AboutUsPics"]["name"]["image"])){
				if(file_exists(bDIR.'/uploads/slider/'. $model->image)) {
					unlink(bDIR . '/uploads/slider/' . $model->image);
				}
				$uploads_dir = UPLOADS.'slider/';
				$tmp_name = $_FILES["AboutUsPics"]["tmp_name"]["image"];
				$name_explode = explode(".",$_FILES["AboutUsPics"]["name"]["image"]);
				$format = end($name_explode);
				$format = strtolower($format);
				$model->image = md5(time()).'.'.$format;

				@move_uploaded_file($tmp_name, "$uploads_dir/$model->image");
			}
			if($model->save())
				$this->redirect(array('admin'));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('AboutUsPics');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new AboutUsPics('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['AboutUsPics']))
			$model->attributes=$_GET['AboutUsPics'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return AboutUsPics the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=AboutUsPics::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param AboutUsPics $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='about-us-pics-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}

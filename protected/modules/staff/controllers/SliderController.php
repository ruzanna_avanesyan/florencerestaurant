<?php

class SliderController extends Controller
{

	public $layout = "/layouts/main";

	public $canWithoutLogin = array();
	function init() {
		$this->canWithoutLogin = array('login');

		if(!isset(Yii::app()->session['stuff'])) {
			if (!in_array($this->action, $this->canWithoutLogin)) {
				$this->redirect($this->CreateUrl('default/login'));
			}
		}
	}

	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Slider;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Slider']))
		{
			if(!empty($_FILES["Slider"]['name']["image"]) ) {
				$uploads_dir = UPLOADS.'slider/';
				$tmp_name = $_FILES["Slider"]["tmp_name"]["image"];
				$name_explode = explode(".",$_FILES["Slider"]["name"]["image"]);
				$format = end($name_explode);
				$model->attributes = $_POST['Slider'];
				$model->image = md5(time()).".".$format;
				if(move_uploaded_file($tmp_name, "$uploads_dir/$model->image")){
					if($model->save())
						$languages = Helpers::languages();
						if(!empty($languages)){
							foreach($languages as $key => $value){
								$model_label = new SliderLabel();
								$model_label->language_id 	= $value->id;
								$model_label->slider_id 	= $model->id;
								$model_label->header 		= $_POST['SliderLabel']['header'][$value->id];
								$model_label->content 		= $_POST['SliderLabel']['content'][$value->id];
								$model_label->save();
							}
						}
						$this->redirect(array('view','id'=>$model->id));
				}
			}


		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Slider']))
		{
			$oldImage = $model->image;
			$model->attributes=$_POST['Slider'];
			$model->image = $oldImage;

			if($model->save())
				if(!empty($_FILES["Slider"]["name"]["image"])){
					if(file_exists(bDIR.'/uploads/slider/'. $model->image)) {
						unlink(bDIR . '/uploads/slider/' . $model->image);
					}
					$uploads_dir = UPLOADS.'slider/';
					$tmp_name = $_FILES["Slider"]["tmp_name"]["image"];
					$name_explode = explode(".",$_FILES["Slider"]["name"]["image"]);
					$format = end($name_explode);
					$format = strtolower($format);
					$model->image = md5(time()).'.'.$format;

					@move_uploaded_file($tmp_name, "$uploads_dir/$model->image");
					$model->save();
				}
				$languages = Helpers::languages();

				foreach($languages as $value){
					$model_label = SliderLabel::model()->findByAttributes(array('slider_id' => $id,'language_id' => $value->id));
					if(empty($model_label)){
						$model_label = new SliderLabel();
					}
					$model_label->language_id 	= $value->id;
					$model_label->slider_id 	= $id;
					$model_label->header		= $_POST['SliderLabel']['header'][$value->id];
					$model_label->content		= $_POST['SliderLabel']['content'][$value->id];
					$model_label->save();

				}
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Slider');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Slider('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Slider']))
			$model->attributes=$_GET['Slider'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Slider the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Slider::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Slider $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='slider-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}

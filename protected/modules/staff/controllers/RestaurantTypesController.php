<?php

class RestaurantTypesController extends Controller
{

	public $layout = "/layouts/main";

	public $canWithoutLogin = array();
	function init() {
		$this->canWithoutLogin = array('login');

		if(!isset(Yii::app()->session['stuff'])) {
			if (!in_array($this->action, $this->canWithoutLogin)) {
				$this->redirect($this->CreateUrl('default/login'));
			}
		}
	}


	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		die;
		$model=new RestaurantTypes;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['RestaurantTypes']))
		{
			$model->attributes=$_POST['RestaurantTypes'];
			if(!empty($_FILES["RestaurantTypes"]['name']["image"]) ) {
				$uploads_dir = UPLOADS.'collective/';
				$tmp_name = $_FILES["RestaurantTypes"]["tmp_name"]["image"];
				$name_explode = explode(".",$_FILES["RestaurantTypes"]["name"]["image"]);
				$format = end($name_explode);
				$model->attributes = $_POST['RestaurantTypes'];
				$model->image = md5(time()).".".$format;
				if(move_uploaded_file($tmp_name, "$uploads_dir/$model->image")){
					if($model->save())
						$languages = Helpers::languages();
					if($languages){
						foreach($languages as $value){
							$model_label = new RestaurantTypesLabel();
							$model_label->language_id = $value->id;
							$model_label->restaurant_types_id = $model->id;
							$model_label->name = $_POST['RestaurantTypesLabel']['name'][$value->id];
							$model_label->small_description = $_POST['RestaurantTypesLabel']['small_description'][$value->id];
							$model_label->description = $_POST['RestaurantTypesLabel']['description'][$value->id];
							$model_label->save();
						}
					}
					$this->redirect(array('view','id'=>$model->id));
				}
			}

		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['RestaurantTypes']))
		{
			$oldImage = $model->image;
			$model->attributes=$_POST['RestaurantTypes'];
			$model->image = $oldImage;
			if(!empty($_FILES["RestaurantTypes"]['name']["image"]) ) {
				if(file_exists(bDIR.'/uploads/collective/'. $model->image)) {
					unlink(bDIR . '/uploads/collective/' . $model->image);
				}
				$uploads_dir = UPLOADS.'collective/';
				$tmp_name = $_FILES["RestaurantTypes"]["tmp_name"]["image"];
				$name_explode = explode(".",$_FILES["RestaurantTypes"]["name"]["image"]);
				$format = end($name_explode);
				$model->image = md5(time()).".".$format;
				if(move_uploaded_file($tmp_name, "$uploads_dir/$model->image")){

					$this->redirect(array('view','id'=>$model->id));
				}
			}
			if($model->save())
				$languages = Helpers::languages();
					if($languages){
						foreach($languages as $value){
							$model_label = RestaurantTypesLabel::model()->findByAttributes(array('language_id' => $value->id, 'restaurant_types_id' => $model->id));
							if(empty($model_label)){
								$model_label = new RestaurantTypesLabel();
							}
							$model_label->language_id = $value->id;
							$model_label->restaurant_types_id = $model->id;
							$model_label->name = $_POST['RestaurantTypesLabel']['name'][$value->id];
							$model_label->small_description = $_POST['RestaurantTypesLabel']['small_description'][$value->id];
							$model_label->description = $_POST['RestaurantTypesLabel']['description'][$value->id];
							$model_label->save();
						}
					}


				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('RestaurantTypes');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new RestaurantTypes('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['RestaurantTypes']))
			$model->attributes=$_GET['RestaurantTypes'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return RestaurantTypes the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=RestaurantTypes::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param RestaurantTypes $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='restaurant-types-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}

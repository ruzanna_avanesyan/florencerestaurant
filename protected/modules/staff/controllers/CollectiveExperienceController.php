<?php

class CollectiveExperienceController extends Controller
{
	public $layout = "/layouts/main";

	public $canWithoutLogin = array();
	function init() {
		$this->canWithoutLogin = array('login');

		if(!isset(Yii::app()->session['stuff'])) {
			if (!in_array($this->action, $this->canWithoutLogin)) {
				$this->redirect($this->CreateUrl('default/login'));
			}
		}
	}


	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new CollectiveExperience;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['CollectiveExperience']))
		{
			if(!empty($_FILES["CollectiveExperience"]['name']["image"]) ) {
				$uploads_dir = UPLOADS . 'collective/';
				$tmp_name = $_FILES["CollectiveExperience"]["tmp_name"]["image"];
				$name_explode = explode(".", $_FILES["CollectiveExperience"]["name"]["image"]);
				$format = end($name_explode);
				$model->attributes = $_POST['CollectiveExperience'];
				$model->image = md5(time()) . "." . $format;
				if (move_uploaded_file($tmp_name, "$uploads_dir/$model->image")) {
					if($model->save())

						$this->redirect(array('view','id'=>$model->id));
				}
			}

		}
		$collective = Collective::model()->with('collectiveLabel')->findAll();

		$this->render('create',array(
			'model'			=> $model,
			'collective'	=> $collective
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['CollectiveExperience']))
		{
			$oldImage = $model->image;
			$model->attributes=$_POST['CollectiveExperience'];
			$model->image = $oldImage;
			if(!empty($_FILES["CollectiveExperience"]['name']["image"]) ) {
				if(file_exists(bDIR.'/uploads/collective/'. $model->image)) {
					unlink(bDIR . '/uploads/collective/' . $model->image);
				}
				$uploads_dir = UPLOADS . 'collective/';
				$tmp_name = $_FILES["CollectiveExperience"]["tmp_name"]["image"];
				$name_explode = explode(".", $_FILES["CollectiveExperience"]["name"]["image"]);
				$format = end($name_explode);
				$model->image = md5(time()) . "." . $format;
				move_uploaded_file($tmp_name, "$uploads_dir/$model->image");
			}
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}
		$collective = Collective::model()->with('collectiveLabel')->findAll();
		$this->render('update',array(
			'model'=>$model,
			'collective'	=> $collective
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('CollectiveExperience');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new CollectiveExperience('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['CollectiveExperience']))
			$model->attributes=$_GET['CollectiveExperience'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return CollectiveExperience the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=CollectiveExperience::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CollectiveExperience $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='collective-experience-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}

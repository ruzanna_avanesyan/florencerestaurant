<?php

class GalleryController extends Controller
{

	public $layout = "/layouts/main";

	public $canWithoutLogin = array();
	function init() {
		$this->canWithoutLogin = array('login');

		if(!isset(Yii::app()->session['stuff'])) {
			if (!in_array($this->action, $this->canWithoutLogin)) {
				$this->redirect($this->CreateUrl('default/login'));
			}
		}
	}


	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Gallery;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Gallery']))
		{
			if(!empty($_FILES['Gallery']['name']['image'])){
				foreach($_FILES['Gallery']['name']['image'] as $key => $value){
					$tmp_name = $_FILES["Gallery"]["tmp_name"]["image"][$key];
					$name_explode = explode(".",$_FILES["Gallery"]["name"]["image"][$key]);
					$uploads_dir = UPLOADS.'gallery/';
					$format = end($name_explode);
					$format = strtolower($format);
					$model = new Gallery();
					$model->category_id = $_POST['Gallery']['category_id'];
					$model->image = md5(time().$name_explode[0]).'.'.$format;
					$model->sort_order = 0;
					if($model->save()){
						$model->sort_order = $model->id;
						$model->save();
						move_uploaded_file($tmp_name, "$uploads_dir/$model->image");
					}

				}
				$this->redirect(array('admin'));
			}

		}

		$category = GalleryCategory::model()->with('galleryCategoryLabel',array('condition' => 'language_id = 1',array('order' => 'sort_order ASC')))->findAll();
		$this->render('create',array(
			'model'		=> $model,
			'category'	=> $category
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Gallery']))
		{
			$oldImage = $model->image;
			$model->attributes=$_POST['Gallery'];
			$model->image = $oldImage;
			if(!empty($_FILES["Gallery"]["name"]["image"])){
				if(file_exists(bDIR.'/uploads/gallery/'. $model->image)) {
					unlink(bDIR . '/uploads/gallery/' . $model->image);
				}
				$uploads_dir = UPLOADS.'gallery/';
				$tmp_name = $_FILES["Gallery"]["tmp_name"]["image"];
				$name_explode = explode(".",$_FILES["Gallery"]["name"]["image"]);
				$format = end($name_explode);
				$format = strtolower($format);
				$model->image = md5(time()).'.'.$format;

				@move_uploaded_file($tmp_name, "$uploads_dir/$model->image");

			}
			$model->save();
				$this->redirect(array('view','id'=>$model->id));
		}
		$category = GalleryCategory::model()->with('galleryCategoryLabel',array('condition' => 'language_id = 1',array('order' => 'sort_order ASC')))->findAll();
		$this->render('update',array(
			'model'=>$model,
			'category'	=> $category
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Gallery');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Gallery('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Gallery']))
			$model->attributes=$_GET['Gallery'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Gallery the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Gallery::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Gallery $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='gallery-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}

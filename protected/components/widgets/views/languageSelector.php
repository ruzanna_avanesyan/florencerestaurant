<?php
foreach($languages as $key=>$lang) {
    $selected = "";
    if($lang->iso == $currentLang) {
        $selected = "active";
    }


        echo "<li class=\"$selected text-uppercase\" >".CHtml::link(
                $lang->iso,
                $this->getOwner()->createMultilanguageReturnUrl($lang->iso))."</li>";
}
?>
<?php
/* @var $this CollectiveExperienceController */
/* @var $model CollectiveExperience */

$this->breadcrumbs=array(
	'Collective Experiences'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List CollectiveExperience', 'url'=>array('index')),
	array('label'=>'Create CollectiveExperience', 'url'=>array('create')),
	array('label'=>'View CollectiveExperience', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage CollectiveExperience', 'url'=>array('admin')),
);
?>

<h1>Update CollectiveExperience <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>
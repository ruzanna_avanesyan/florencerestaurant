<?php
/* @var $this CollectiveExperienceController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Collective Experiences',
);

$this->menu=array(
	array('label'=>'Create CollectiveExperience', 'url'=>array('create')),
	array('label'=>'Manage CollectiveExperience', 'url'=>array('admin')),
);
?>

<h1>Collective Experiences</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>

<?php
/* @var $this CollectiveExperienceController */
/* @var $model CollectiveExperience */

$this->breadcrumbs=array(
	'Collective Experiences'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List CollectiveExperience', 'url'=>array('index')),
	array('label'=>'Create CollectiveExperience', 'url'=>array('create')),
	array('label'=>'Update CollectiveExperience', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete CollectiveExperience', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage CollectiveExperience', 'url'=>array('admin')),
);
?>

<h1>View CollectiveExperience #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'collective_id',
		'image',
	),
)); ?>

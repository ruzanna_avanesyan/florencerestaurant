<!doctype html>
<html class="no-js" lang="">
<head>
	<meta charset="utf-8">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title>Florence Restaurant</title>
	<link rel="shortcut icon" href="<?=Yii::app()->request->BaseUrl?>/vendor/img/favicon.ico" type="image/x-icon">
	<meta name="description" content="florence restaurant, armenia, luxe, yerevan, wine, wedding, birthday, italy, halls, Gallery, Events, Menu, Master Chef, coffee bar,">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- Place favicon.ico in the root directory -->

	<!--All Google Fonts-->
	<link href='https://fonts.googleapis.com/css?family=Lora:400,700' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Great+Vibes' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Lato:400,700' rel='stylesheet' type='text/css'>
	<link href="https://fonts.googleapis.com/css?family=Lobster" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Marck+Script" rel="stylesheet">
	<!-- all css here -->
	<!-- bootstrap v3.3.6 css -->
	<link rel="stylesheet" href="<?=Yii::app()->request->BaseUrl?>/vendor/css/bootstrap.min.css">
	<!-- animate css -->
	<link rel="stylesheet" href="<?=Yii::app()->request->BaseUrl?>/vendor/css/animate.css">
	<!-- jquery-ui.min css -->
	<link rel="stylesheet" href="<?=Yii::app()->request->BaseUrl?>/vendor/css/jquery-ui.min.css">
	<!-- meanmenu css -->
	<link rel="stylesheet" href="<?=Yii::app()->request->BaseUrl?>/vendor/css/meanmenu.min.css">
	<!-- owl.carousel css -->
	<link rel="stylesheet" href="<?=Yii::app()->request->BaseUrl?>/vendor/css/owl.carousel.css">
	<!-- font-awesome css -->
	<link rel="stylesheet" href="<?=Yii::app()->request->BaseUrl?>/vendor/css/font-awesome.min.css">
	<!-- datepicker css -->
	<link rel="stylesheet" href="<?=Yii::app()->request->BaseUrl?>/vendor/css/bootstrap-datepicker.css">
	<!-- timepicker css -->
	<link rel="stylesheet" href="<?=Yii::app()->request->BaseUrl?>/vendor/css/jquery.timepicker.css">
	<!-- nivo-slider css -->
	<link rel="stylesheet" href="<?=Yii::app()->request->BaseUrl?>/vendor/lib/css/nivo-slider.css">
	<!-- venobox css -->
	<link rel="stylesheet" href="<?=Yii::app()->request->BaseUrl?>/vendor/lib/venobox/venobox.css">

	<link rel="stylesheet" href="<?=Yii::app()->request->BaseUrl?>/vendor/owl-carousel/owl.carousel.css">
	<!-- style css -->
	<link rel="stylesheet" href="<?=Yii::app()->request->BaseUrl?>/vendor/css/style.css">


	<!-- responsive css -->
	<link rel="stylesheet" href="<?=Yii::app()->request->BaseUrl?>/vendor/css/responsive.css">
	<!-- modernizr css -->

	<link rel="stylesheet" href="<?=Yii::app()->request->BaseUrl?>/vendor/css/app.css">
	<link rel="stylesheet" href="<?=Yii::app()->request->BaseUrl?>/vendor/css/elixir.css"/>
	<link rel="stylesheet" href="<?=Yii::app()->request->BaseUrl?>/vendor/css/swipebox.css">

	<link rel="stylesheet" href="<?=Yii::app()->request->BaseUrl?>/vendor/css/mystyle.css">
	<link rel="stylesheet" href="<?=Yii::app()->request->BaseUrl?>/vendor/css/styles.css">
	<link rel="stylesheet" href="<?=Yii::app()->request->BaseUrl?>/vendor/magnific-popup/magnific-popup.css">

	<link rel="stylesheet" href="<?=Yii::app()->request->BaseUrl?>/vendor/css/lightbox.min.css">
	<script src="<?=Yii::app()->request->BaseUrl?>/vendor/js/jquery.min.js"></script>
	<script src="<?=Yii::app()->request->BaseUrl?>/vendor/js/custom.js"></script>
	<script src="<?=Yii::app()->request->BaseUrl?>/vendor/js/jquery.meanmenu.js"></script>




</head>
<body  data-spy="scroll" >
<div class="loader preloader" id="preloader">
	<div>
		<img src="<?php echo Yii::app()->request->BaseUrl?>/vendor/img/default.gif" alt="preloader">
	</div>
</div>
<?php echo $content; ?>
<script src="<?=Yii::app()->request->BaseUrl?>/vendor/js/lightbox.js"></script>
<!-- all js here -->
<!-- jquery latest version -->
<!-- bootstrap js -->
<script src="<?=Yii::app()->request->BaseUrl?>/vendor/magnific-popup/jquery.magnific-popup.min.js"></script>
<script src="<?=Yii::app()->request->BaseUrl?>/vendor/js/bootstrap.min.js"></script>
<!-- owl.carousel js -->
<script src="<?=Yii::app()->request->BaseUrl?>/vendor/js/owl.carousel.min.js"></script>
<!-- datepicker js -->
<script src="<?=Yii::app()->request->BaseUrl?>/vendor/js/bootstrap-datepicker.js"></script>
<!-- timepicker js -->
<script src="<?=Yii::app()->request->BaseUrl?>/vendor/js/jquery.timepicker.min.js"></script>
<!-- meanmenu js -->
<!-- jquery-ui js -->
<script src="<?=Yii::app()->request->BaseUrl?>/vendor/js/jquery-ui.min.js"></script>
<!-- nivo.slider js -->
<script src="<?=Yii::app()->request->BaseUrl?>/vendor/lib/js/jquery.nivo.slider.pack.js"></script>
<script src="<?=Yii::app()->request->BaseUrl?>/vendor/lib/js/nivo-active.js"></script>
<!-- wow js -->
<script src="<?=Yii::app()->request->BaseUrl?>/vendor/js/wow.min.js"></script>
<!-- Sticky JS -->
<script src="<?=Yii::app()->request->BaseUrl?>/vendor/js/jquery.sticky.js"></script>
<!-- venobox js -->
<script src="<?=Yii::app()->request->BaseUrl?>/vendor/lib/venobox/venobox.js"></script>
<!-- Scroll up js -->
<script src="<?=Yii::app()->request->BaseUrl?>/vendor/js/jquery.scrollUp.min.js"></script>
<!-- google map  js -->
<script type="text/javascript"
		src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDCtwnuOqoLcZNbGbusMYmg0Fq3swrczwA&callback=initMap" async defer></script>

<!-- plugins js -->
<script src="<?=Yii::app()->request->BaseUrl?>/vendor/js/plugins.js"></script>
<!-- main js -->
<script src="<?=Yii::app()->request->BaseUrl?>/vendor/js/main.js"></script>

<script src="<?=Yii::app()->request->BaseUrl?>/vendor/js/vertical_navigation.js"></script>
<script src="<?=Yii::app()->request->BaseUrl?>/vendor/js/attributes.js"></script>
<script>
	$(".nav-menu").vm_img({
		path_img: "<?=Yii::app()->request->BaseUrl?>/vendor/img/"
	});
</script>


<script src="<?=Yii::app()->request->BaseUrl?>/vendor/js/default.js"></script>
<!--<script src="js/plugins.js"></script>-->
<script type="text/javascript" src="<?=Yii::app()->request->BaseUrl?>/vendor/js/jquery.isotope.min.js"></script>

<script>
	$(document).ready(function(){
		$('#carousel').owlCarousel({
			items: 6,
			autoPlay: false,
			navigation: true,
			navigationText: ['<i class="fa fa-chevron-left fa-5x"></i>', '<i class="fa fa-chevron-right fa-5x"></i>'],
			pagination: false
		});

	});
</script>
<script>
	$(document).ready(function(){
		$('.item a').click(function (e) {
			e.preventDefault();
			$(this).tab('show');
		});
	});
</script>
<script>
	$(window).resize(function(){
		$('.for-slider-parallax').css({
			top : $('.slider-container').height()
		});
	});



</script>

</body>
</html>

<div class="header-slider-area">
	<!-- mobile-menu-area start -->
	<div class="mobile-menu-area">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<nav id="dropdown" class="nav-menu">
						<ul>
							<li>
								<a href="<?=$this->CreateUrl('site/index')?>">
									<i class="fa fa-home" aria-hidden="true"></i>
								</a>
							</li>
							<?php
							if(!empty($data['menu'])):
								foreach($data['menu'] as $key => $value):
									?>
									<li>
										<a href="<?=$value->url?>" class="item_menu">
											<?=$value->menuLabels[0]->value?>
										</a>
									</li>
									<?php
								endforeach;
							endif;
							?>

						</ul>
					</nav>
				</div>
			</div>
		</div>
	</div>
	<!--mobile menu area end-->

	<!--header menu area are start-->
	<div class="header-menu-area">
		<!--header-area are start-->
		<div class="header">
			<div class="container">
				<div class="row">
					<div class="col-md-4 col-sm-4  hidden-xs">
						<div class="search-phone">
							<ul>
								<li>
									<a class="phone no_pointer" href="#"><i class="fa fa-phone" aria-hidden="true"></i>
										<?=$data['contacts']->phone?>
									</a>
								</li>
							</ul>
						</div>
					</div>
					<div class="col-md-4 col-sm-4">
						<div class="logo">
							<a href="<?=$this->CreateUrl('site/index')?>">
								<img src="<?=Yii::app()->request->BaseUrl?>/vendor/img/florence_logo_1.png" alt="florence logo">
							</a>
						</div>
					</div>
					<div class="col-md-4 col-sm-4 hidden-xs">
						<div class="social-icon">
							<ul>
								<?php
									if(!empty($data['follow_us'])):
										foreach($data['follow_us'] as $key => $value):
								?>
											<li>
												<a href="<?=$value->url?>" target="_blank">
													<i class="<?=$value->fa_icon?>" aria-hidden="true"></i>
												</a>
											</li>
								<?php
										endforeach;
									endif;
								?>

								<li class="text-uppercase">
									<div class="dropdown">
										<a id="dLabel" data-target="#" href="#" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
											<?= $this->currentLangIso ?>
											<span class="caret"></span>
										</a>
										<ul class="dropdown-menu languages" aria-labelledby="dLabel">
											<?php
											$this->widget('application.components.widgets.LanguageSelector');
											?>
										</ul>
									</div>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--header-area are end-->

		<!-- Main menu area are start  -->
		<div id="sticker" class="menu-area">
			<div class="container menu-container">
				<div class="row">
					<div class="col-md-12">
						<div class="logo2">
							<a href="<?=$this->CreateUrl('site/index')?>"><img src="<?=Yii::app()->request->BaseUrl?>/vendor/img/florence_logo.png" alt="florence logo"></a>
						</div>
						<div class="main-menu">
							<nav class="nav-menu">
								<ul>
									<li>
										<a href="<?=$this->CreateUrl('site/index')?>">
											<i class="fa fa-home" aria-hidden="true"></i>
										</a>
									</li>
									<?php
										if(!empty($data['menu'])):
											foreach($data['menu'] as $key => $value):
									?>
											<li>
												<a href="<?=$value->url?>" class="item_menu">
													<?=$value->menuLabels[0]->value?>
												</a>
											</li>
									<?php
											endforeach;
										endif;
									?>


								</ul>
							</nav>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- Main menu area are end  -->
	</div>
	<!--header menu area are end-->

	<!-- home slider start -->
	<div class="slider-container">
		<div class="bg-shodow"></div>
		<!-- Slider Image -->
		<div id="mainSlider" class="nivoSlider slider-image">
			<?php
			if($data['slider']):
				foreach($data['slider'] as $key => $value):
					?>
					<img src="<?=THUMB?><?=Yii::app()->request->BaseUrl?>/uploads/slider/<?=$value->image?>&w=1920&h=900" alt="florence slider" title="#htmlcaption<?=$value->id?>"/>
					<?php
				endforeach;
			endif;
			?>
		</div>
		<!-- Slider Caption 1 -->

		<div id="htmlcaption" class="nivo-html-caption slider-caption-1">
			<div class="slider-progress"></div>

			<?php
				if($data['slider']):
					foreach($data['slider'] as $key => $value):
			?>
						<div id="htmlcaption<?=$value->id?>" class="nivo-html-caption slider-caption-<?=$value->id?>">
							<div class="slider-progress"></div>
							<div class="slide1-text">
								<div class="top-icon sl-icon">
								</div>
								<div class="middle-text">
									<div class="cap-title cap-title-slider wow bounceInRight" data-wow-duration="1.2s" data-wow-delay="0.2s">
										<h1 class="text-uppercase"><?=$value->sliderLabels[0]->header?></h1>
									</div>
									<div class="cap-dec cap-dec-slider wow bounceInDown" data-wow-duration="0.9s" data-wow-delay="0s">
										<h3 class="text-uppercase"><?=$value->sliderLabels[0]->content?></h3>
									</div>
									<div class="bottom-icon sl-icon">
										<img src="<?=Yii::app()->request->BaseUrl?>/vendor/img/slider_rou.png" alt="florence slider_rou">
									</div>
								</div>
							</div>
						</div>
			<?php
					endforeach;
				endif;
			?>

		</div>
		<!-- home slider end -->

	</div>
</div>

<div class="for-slider-parallax1">
	<!--about area are start-->
	<div id="halls" class="about-area section">
		<div class="single-about clickable-row" data-href="gallery">

			<img src="<?=THUMB?><?=Yii::app()->request->BaseUrl?>/uploads/slider/<?=$data['aboutUsPics'][0]->image?>&w=640&h=632" alt="about us">
			<div class="about-content" id="about_us">
				<div class="about-table">
					<div class="about-cell cap-title halls-cell">
						<table>
							<tr>
								<td>
                                <span>
                                    <img src="<?=THUMB?><?=Yii::app()->request->BaseUrl?>/vendor/img/flower_left.png&w=56&h=66" alt="flower" class="img-resp">
                                </span>
								</td>
								<td class="halls">
									<h1>
										<a href="#gallery" class="without_attribute item_menu"><?=$this->translation['halls']?></a>
									</h1>
								</td>
								<td>
                                 <span>

                                    <img src="<?=THUMB?><?=Yii::app()->request->BaseUrl?>/vendor/img/flower_right.png&w=56&h=66" alt="flower_right" class="img-resp">
                                </span>
								</td>
							</tr>
						</table>
					</div>
				</div>
			</div>
		</div>
		<div class="single-about sab-2">
			<img src="<?=Yii::app()->request->BaseUrl?>/vendor/img/banner/about%20us.png" alt="about us">
			<div class="about-content">
				<div class="about-table">
					<div class="about-cell about-us">
						<div class="cap-title">
							<h1>
							</h1>
						</div>
						<div class="about-us-title">
							<?=$data['aboutUs']->aboutUsLabel->header?>
						</div>
						<div class="text-center">
							<img src="<?=Yii::app()->request->BaseUrl?>/vendor/img/lines1.png" alt="lines" />
						</div>
						<div class="about-us-description-hide">
							<p>
								<?=$data['aboutUs']->aboutUsLabel->small_description?>

							</p>
						</div>
						<a class="com-btn open-popup-link" href="#aboutUsPopup" >
							<?=$this->translation['read_more']?>
						</a>
					</div>
				</div>
			</div>
		</div>
		<div class="single-about clickable-row" data-href="section_menu">
			<img src="<?=THUMB?><?=Yii::app()->request->BaseUrl?>/uploads/slider/<?=$data['aboutUsPics'][1]->image?>&w640&h=632" alt="florence restaurant">
			<div class="about-content">
				<div class="about-table">
					<div class="about-cell cap-title halls-cell">
						<table>
							<tr>
								<td>
                                <span>
                                    <img src="<?=THUMB?><?=Yii::app()->request->BaseUrl?>/vendor/img/flower_left.png&w=56&h=66" alt="flower left" class="img-resp">
                                </span>
								</td>
								<td class="halls">
									<h1>
										<a href="#section_menu" class="without_attribute item_menu">
											<p><?=$this->translation['master_chef']?></p>
										</a>
									</h1>
								</td>
								<td>
                                 <span>
                                    <img src="<?=THUMB?><?=Yii::app()->request->BaseUrl?>/vendor/img/flower_right.png&w=56&h=66" alt="flower right" class="img-resp">
                                </span>
								</td>
							</tr>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script>
		jQuery(document).ready(function($) {

			$(".clickable-row").click(function() {
				var id = $(this).data("href");
				$('html, body').animate({ scrollTop: $('#'+id).offset().top }, 'slow');
			});
		});
	</script>

	<!--about area are end-->
	<!-- Modal -->
	<div class="single-about-popup mfp-hide" data-effect="fadeIn" id="aboutUsPopup" role="dialog">
		<div class="">
				<div class="about-table">
					<div class="about-cell about-us">
						<div class="about-us-title  about-us-title-popup event_popup_txt">
							<?=$data['aboutUs']->aboutUsLabel->header?>
						</div>
						<div class="text-center">
							<img src="<?=Yii::app()->request->BaseUrl?>/vendor/img/lines1.png" alt="lines" />
						</div>
						<div class="about-us-description">
							<?=$data['aboutUs']->aboutUsLabel->description?>
						</div>
					</div>
				</div>
		</div>
	</div>


	<!-- service holder area are start-->
	<div id="our_team" class="service-holder-area section pt60 pb60">
		<div class="container">
			<div class="row">
				<div class="total-service-holder carsoule-btn">
					<?php
						if(!empty($data['collective'])):
							foreach($data['collective'] as $key => $value):
					?>
								<div class="single-service-holder">
									<div class="col-md-5 col-sm-5 col-xs-12">
										<div class="holder-img">
											<img src="<?=THUMB?><?=Yii::app()->request->BaseUrl?>/uploads/collective/<?=$value->image?>&w=442&h=443" alt="collective">
										</div>
									</div>
									<div class="col-md-7 col-sm-7 col-xs-12">
										<div class="holder-content sm-des">
											<div class="hc-title">
												<h2>
													<?=$value->collectiveLabel->first_name.' '.$value->collectiveLabel->last_name?>
												</h2>
											</div>
											<div class="deginaton">
												<span class="text-uppercase"><?=$value->collectiveLabel->position?></span>
											</div>
											<?=$value->collectiveLabel->description?>

										</div>
										<div class="holder-icon">
											<?php
												if(!empty($value->collectiveExperiences)):
													foreach($value->collectiveExperiences as $elems):
											?>
													<div class="single-icon">
														<img src="<?=THUMB?><?=Yii::app()->request->BaseUrl?>/uploads/collective/<?=$elems->image?>&w=162&h=107" alt="collective">
													</div>
											<?php
													endforeach;
												endif;
											?>

										</div>
									</div>
								</div>
					<?php
							endforeach;
						endif;
					?>

				</div>
				<div class="clearfix"></div>
				<div class="service-area">
					<?php
						if(!empty($data['res_type'])):
							foreach($data['res_type'] as $key => $value):
								$margin_class = "";
								if($key == 0){
									$margin_class = "style='margin:30px 0'";
								}
					?>
						<div class="col-md-4 col-sm-6 col-xs-12">
							<div class="single-service text-center" >

								<div class="service-icon" <?=$margin_class?>>
									<img src="<?=Yii::app()->request->BaseUrl?>/uploads/collective/<?=$value->image?>" alt="collective">
								</div>
								<div class="service-title">
									<h2><?=$value->restaurantTypesLabel->name?></h2>
								</div>
								<div class="service-text sm-des">
									<?=$value->restaurantTypesLabel->small_description?>
								</div>
								<div class="read-btn">
									<a class="open-popup-link" href="#resType<?=$value->id?>" >
										<?=$this->translation['read_more']?>
									</a>
								</div>
							</div>
						</div>
						<div class="single-about-popup mfp-hide" data-effect="fadeIn" id="resType<?=$value->id?>" role="dialog">
							<div class="">
								<div class="single-service text-center">
									<div class="service-icon" style="margin: 30px 0">
										<img src="<?=Yii::app()->request->BaseUrl?>/uploads/collective/<?=$value->image?>" alt="collective">
									</div>
									<div class="about-us-title event_popup_txt">
										<h2><?=$value->restaurantTypesLabel->name?></h2>
									</div>
									<div class="about-us-description">
										<?=$value->restaurantTypesLabel->small_description?>
									</div>

								</div>
							</div>
						</div>
					<?php
							endforeach;
						endif;
					?>

				</div>
			</div>
		</div>
	</div>
	<!-- service holder area are end-->

	<!-- our menu area are start-->
	<div id="section_menu" class="our-menu-area section  pt60 pb60">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="section-title text-center pb40">
						<div>
							<img src="<?=Yii::app()->request->BaseUrl?>/vendor/img/flower.png" alt="flower">
						</div>
						<h1><?=$this->translation['menu']?></h1>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="total-our-menu carsoule-btn cb-2">
					<?php
						if(!empty($data['food_menu'])):
							foreach($data['food_menu'] as $key => $value):
					?>
							<div class="col-xs-12">
								<div class="single-our-menu">
									<div class="sm-image">
<!--										<a href="" class="no_pointer">-->
											<img src="<?=Yii::app()->request->BaseUrl?>/uploads/menu/<?=$value->image?>" alt="menu">
<!--										</a>-->
										<div class="sm-heading menu_name">
											<?=$value->foodMenuLabels[0]->name?>
										</div>

									</div>
									<div class="text-center">
										<img src="<?=Yii::app()->request->BaseUrl?>/vendor/img/lines.png" alt="lines" />
									</div>
									<div class="sm-content">
										<div class="sm-des">
											<?=$value->foodMenuLabels[0]->description?>
										</div>
									</div>
								</div>
							</div>
					<?php
							endforeach;
						endif;
					?>

				</div>
			</div>
			<!--<div class="find-more text-center">-->
			<!--<a class="com-btn" href="#">Find Out More</a>-->
			<!--</div>-->
		</div>
	</div>
	<!-- our menu area are end-->


	<!-- our-special-menu-area are start-->
	<div id="gallery" class="our-special-menu-area section">
		<div class="bg pt60">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="section-title text-center pb40">
							<img src="<?=Yii::app()->request->BaseUrl?>/vendor/img/flower.png" alt="flower">
							<h1><?=$this->translation['gallery']?></h1>
<!--							<nav class="primary">-->

							<ul class="list-inline" id="gallery_data">
								<li class="active"><a data-toggle="tab" class="" href="#all" data-href="all"><span>All</span></a></li>
								<?php
								if(!empty($data['gal_category'])):
									foreach($data['gal_category'] as $key => $value):
										?>
										<li class=""><a data-toggle="tab" data-href="gallery_<?=$value->id?>" href="#gallery_<?=$value->id?>"><span><?=$value->galleryCategoryLabel->name?></span></a></li>
										<?php
									endforeach;
								endif;
								?>
							</ul>

<!--							</nav>-->
						</div>
					</div>
				</div>
			</div>
		</div>
		<section  class="gallery dark">
			<div class="jt_row jt_row-fluid row">
				<div class="col-md-12 jt_col column_container">
					<div class="tab-content gallery_tab_content">
								<div id="all" class="tab-pane tab-pane-gallery tab-pane-gallery-tabs active ">
									<div class="images_all pull-left">
									<?php
										if(!empty($data['all_gallery'])):

											foreach($data['all_gallery'] as $key => $value):
									?>
												<article class="entry1 all without_padding col-xs-12 col-md-4">

														<a class="image" data-lightbox="roadtrip" href="<?=Yii::app()->request->BaseUrl?>/uploads/gallery/<?=$value->image?>">
															<span class="roll" ></span>
															<img src="<?=THUMB?><?=Yii::app()->request->BaseUrl?>/uploads/gallery/<?=$value->image?>&w=640&h=477" class="img-responsive magnifier1 animated zoomIn" alt="gallery"/>
														</a>
												</article>
									<?php
											endforeach;
										endif;
									?>
									</div>
									<div class="input-box checkout gallery">
										<a class="com-btn  gallery_all gallery_load_more" data-category = "all" data-offset="1" href="#"><?=strip_tags($this->translation['load_more'])?></a>
									</div>
								</div>
						<?php
						if(!empty($data['gallery'])):
							foreach($data['gallery'] as $key => $value):
								?>
								<div id="gallery_<?=$key?>" class="tab-pane tab-pane-gallery-tabs">
									<div class="images_<?=$key?> pull-left">
										<?php foreach($value as $elems => $item):

										?>
											<article class="without_padding col-xs-12 col-md-4">
												<a class="image" data-lightbox="roadtrip"  href="<?=Yii::app()->request->BaseUrl?>/uploads/gallery/<?=$item->image?>">
													<span class="roll" ></span>
													<img src="<?=THUMB?><?=Yii::app()->request->BaseUrl?>/uploads/gallery/<?=$item->image?>&w=640&h=477" class="img-responsive magnifier1  animated zoomIn" alt="gallery"/>
												</a>
											</article>
										<?php endforeach;?>
									</div>
									<div class="input-box checkout gallery">
										<a class="com-btn  gallery_<?=$key?> gallery_load_more" data-category = "<?=$key?>" data-offset="1" href="#" ><?=strip_tags($this->translation['load_more'])?></a>
									</div>
								</div>

								<?php
							endforeach;
						endif;
						?>


						</div>
				</div> <!-- End .jt_col -->
			</div> <!-- End .jt_row -->
			<div class="voffset200"></div>
		</section>
	</div>
	<script>
		$(document).ready(function(){
			lightbox.option({
				'resizeDuration': 200,
				'wrapAround': true,
				'alwaysShowNavOnTouchDevices':true,
				'showImageNumberLabel':false
			});
		})

		$(document).on('click','.gallery_load_more',function(e){
			e.preventDefault();
			var category = $(this).data('category');
			var offset = $(this).attr('data-offset');
			$.ajax({
				url:'<?=$this->CreateUrl('site/getImages')?>',
				data:{offset:offset,category:category},
				type:'POST',
				dataType:'JSON',
				success:function(result){

					$.each(result['gallery'],function(index,value){
						var strVar="";
						strVar += "<article class=\"entry1 all without_padding col-lg-4 col-md-4 col-xs-12\">";
						strVar += "												<a class=\"image swipebox\" data-lightbox=\"roadtrip\" href=\"<?=Yii::app()->request->BaseUrl?>\/uploads\/gallery\/"+value['image']+"\">";
						strVar += "													<span class=\"roll\" ><\/span>";
						strVar += "													<img src=\"<?=THUMB?><?=Yii::app()->request->BaseUrl?>\/uploads\/gallery\/"+value['image']+"&w=640&h=477\" class=\"img-responsive magnifier1 animated zoomIn\" alt=\"gallery\"\/>";
						strVar += "												<\/a>";
						strVar += "	<\/article>";
						$('.images_'+category).append(strVar)
					});
					$('.gallery_'+category).attr('data-offset',result['offset']);
					if(!result['next_step']){
						$('.gallery_'+category).addClass('hide');
					}

					var width = $('.magnifier1').width();
					var height = $('.magnifier1').height();
					$('.roll').css({width:width+'px',height:height+'px'})
					rolls();
//					$(".gallery a").tosrus();
				}
			})
		});

	</script>
	<!-- our-special-menu-area are end-->

	<!--Reservation area start-->
	<div id="events" class="reservation-area section pt60 pb60">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<div class="events section-title text-center pb40">
						<img src="<?=Yii::app()->request->BaseUrl?>/vendor/img/flower_white.png" alt="flower white" />
						<h1><?=$this->translation['events']?></h1>
					</div>
				</div>
			</div>
			<div class="smi-des text-center">
            <span>
				<?=$this->translation['events_txt']?>
            </span>
			</div>
			<div class="row event-tab">
				<div id="carousel" class="nav nav-tabs owl-carousel" role="tablist">
					<?php
						if(!empty($data['events_days'])):
							foreach($data['events_days'] as $key => $value):
								$active = "";
								if($key == 0){
									$active = "active";
								}
					?>
								<div role="presentation" class="<?=$active?> item text-center">
									<a href="#events_<?=date('Y-m-d',strtotime($value->date))?>" aria-controls="events_<?=date('Y-m-d',strtotime($value->date))?>" role="tab" data-toggle="tab">
										<?=date('d',strtotime($value->date)).' '.strip_tags($this->translation['month_'.date('m',strtotime($value->date))]).' '.date('Y', strtotime($value->date))?>
									</a>
								</div>
					<?php
							endforeach;
						endif;
					?>

				</div>
				<?php
					if(!empty($data['events'])):
				?>
				<div class="slider-reserve tab-content">
					<?php


							foreach($data['events'] as $key => $value):
								$active = "";
								if($key == 0){
									$active = "active";
								}
					?>
						<div role="tabpanel" class="transition tab-pane <?=$active?>" id="events_<?=date('Y-m-d',strtotime($value->date))?>">
							<div class="col-md-6 col-sm-12 col-xs-12">
								<div class="reserve-slide carasourl-pagi">
									<div class="rsa-img">
										<img src="<?=THUMB?><?=Yii::app()->request->BaseUrl?>/uploads/events/<?=$value->image?>&w=536&h=328" alt="events">
									</div>
								</div>
							</div>
							<div class="col-md-6 col-sm-12 col-xs-12">
								<div class="reserve-table">
									<div>
										<div class="text-uppercase event-name">
											<?=$value->eventsLabel->name?>
										</div>
										<div class="event-date">
											<?=date('d',strtotime($value->date)).' '.strip_tags($this->translation['month_'.date('m',strtotime($value->date))]).' '.date('Y', strtotime($value->date)).', '.date('H:i', strtotime($value->date))?>
										</div>
										<div class="event-description">
											<?=$value->eventsLabel->small_description?>
										</div>
									</div>
									<div class="input-box checkout events">

										<a class="com-btn open-popup-link" href="#eventPopup<?=$value->id?>" ><?=$this->translation['read_more']?></a>
									</div>
								</div>
							</div>
						</div>

								<div class="white-popup mfp-hide" data-effect="fadeIn" id="eventPopup<?=$value->id?>" role="dialog">
									<div class="">
										<div class="about-table">
											<div class="images_event">
												<img src="<?=THUMB?><?php echo Yii::app()->request->BaseUrl?>/uploads/events/<?=$value->image?>&w=536&h=328" alt="events">
											</div>

										</div>
										<div class="about-cell about-us">
											<div class="events_title event_popup_txt event_popup_txt about-us-title-popup">
												<?=$value->eventsLabel->name?>
											</div>
											<div class="text-center">
												<img src="<?=Yii::app()->request->BaseUrl?>/vendor/img/lines1.png" alt="lines" />
											</div>
											<div class="about-us-description event_popup_txt">
												<?=$value->eventsLabel->description?>
											</div>
										</div>
									</div>
								</div>

					<?php
							endforeach;

					?>
				</div>
				<?php endif;?>
			</div>
		</div>
	</div>
	<!--Reservation area end-->

	<!--map area are start-->
	<div id="find_us" class="map-area section">
		<div id="googleMap"></div>
		<script type="text/javascript">

			var url = '<?=Yii::app()->request->BaseUrl?>';
			var map;
			var myLatLng = {lat: <?=$data['marker']->lat?>, lng: <?=$data['marker']->lng?>};
			function CenterControl(controlDiv, map) {

				// Set CSS for the control border.
				var controlUI = document.createElement('div');
				controlUI.style.backgroundColor = 'rgb(30, 57, 64)';
				controlUI.style.border = '2px solid rgb(30, 57, 64)';
				controlUI.style.borderRadius = '3px';
				controlUI.style.boxShadow = '0 2px 6px rgba(0,0,0,.3)';
				controlUI.style.cursor = 'pointer';
				controlUI.style.marginTop = '10px';
				controlUI.style.textAlign = 'center';
				controlUI.title = 'Route';
				controlUI.id = 'routing_btn';
				controlDiv.appendChild(controlUI);

				// Set CSS for the control interior.
				var controlText = document.createElement('div');
				controlText.style.color = 'rgb(197, 162, 104)';
				controlText.style.fontFamily = 'Roboto,Arial,sans-serif';
				controlText.style.fontSize = '16px';
				controlText.style.lineHeight = '38px';
				controlText.style.paddingLeft = '20px';
				controlText.style.paddingRight = '20px';
				controlText.innerHTML = 'Route';
				controlUI.appendChild(controlText);

				// Setup the click event listeners: simply set the map to Chicago.
				controlUI.addEventListener('click', function() {
					var Center =  {lat: <?=$data['marker']->lat?>, lng: <?=$data['marker']->lng?>};
					var directionsDisplay;
					var directionsService = new google.maps.DirectionsService();

					var infoWindow = new google.maps.InfoWindow({map: map});
					if (navigator.geolocation) {
						navigator.geolocation.getCurrentPosition(function(position) {
init
							var pos = {
								lat: position.coords.latitude,
								lng: position.coords.longitude
							};

//							infoWindow.setPosition(pos);
//							infoWindow.setContent('Your location');
							map.setCenter(pos);
							var directionsService = new google.maps.DirectionsService;
							var directionsDisplay = new google.maps.DirectionsRenderer;

							directionsDisplay.setMap(map);


							Route(directionsService,directionsDisplay,pos);
						}, function() {
							handleLocationError(true, infoWindow, map.getCenter());
						});
					} else {
						handleLocationError(false, infoWindow, map.getCenter());
					}
				});

			}



			function initMap() {
				var myLatLng = {lat: <?=$data['marker']->lat?>, lng: <?=$data['marker']->lng?>};

				map = new google.maps.Map(document.getElementById('googleMap'), {

					zoom: 16,
					center: myLatLng,
					scrollwheel: false,



				});

				var centerControlDiv = document.createElement('div');
				var centerControl = new CenterControl(centerControlDiv, map);

				centerControlDiv.index = 1;
				map.controls[google.maps.ControlPosition.TOP_CENTER].push(centerControlDiv);

				var result = markers();
				var markers;
				function markers(){
					$.ajax({
						url:'<?=$this->CreateUrl('/site/markers')?>',
						async:false,
						dataType:'json',
						success:function(result){

							markers = result;

							for (var prop in markers) {
								var myLatLng = {lat: parseFloat(markers[prop]['lat']), lng: parseFloat(markers[prop]['lng'])};

								var marker = new google.maps.Marker({
									position: myLatLng,
									animation: google.maps.Animation.BOUNCE,
									icon: '<?=Yii::app()->request->BaseUrl?>/vendor/img/map-marker.png',
									map: map,
								});
								console.log(marker.getPosition());
							}
						}
					})
				}



			}





			function Route(directionsService,directionsDisplay,position) {

				var start = new google.maps.LatLng(position['lat'], position['lng']);
				console.log(start);
				var end = new google.maps.LatLng(40.194687, 44.482011);
				var request = {
					origin: start,
					destination: end,
					travelMode: google.maps.TravelMode.WALKING
				};
				directionsService.route(request, function(result, status) {
					if (status == google.maps.DirectionsStatus.OK) {
						directionsDisplay.setDirections(result);
					} else {
						alert("couldn't get directions:" + status);
					}
				});
			}

		</script>
	</div>
	<!--map area are end-->



	<!--footer area are start-->
	<div id="contacts" class="footer-area section">

		<div class="contacts-content footer-background">
			<div class="row">
				<div class="col-md-4 col-sm-4 col-xs-12">
					<div class="single-footer contacts text-center">
						<div class="sosa-icon">
							<i class="fa fa-phone"></i>
						</div>
						<h1><?=$this->translation['contacts']?></h1>
						<div class="address">
                        <span>
                              <?=$data['contacts']->contactUsLabels[0]->address?>
                        </span>
                        <span class="email">
							 <?=$data['contacts']->email?>
                        </span>
							<div class="phone-no">

								<span><?=$data['contacts']->phone?></span>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-12">
					<div class="single-footer footer-middle text-center">
						<div class="sosa-icon">
							<img src="<?=Yii::app()->request->BaseUrl?>/vendor/img/openinghours.png" alt="contact us">
						</div>
						<h1><?=strip_tags($this->translation['opening_time'])?></h1>
						<div class="service-time">
							<table>
								<?php
									if(!empty($data['opening_keys'])):
										foreach($data['opening_keys'] as $key => $value):
								?>
											<tr>
												<td> <?=$value->openingKeysLabel->name?> : </td>
												<td> <?=$value->time?></td>
											</tr>
								<?php
										endforeach;
									endif;
								?>
							</table>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-12">
					<div class="single-footer text-center">
						<div class="sosa-icon">
							<img src="<?=Yii::app()->request->BaseUrl?>/vendor/img/subscripe.png" alt="subcribe">
						</div>
						<h1><?=strip_tags($this->translation['subscribe_us'])?></h1>
						<div class="detail-subs">
							<?=$this->translation['subscribe_us_txt']?>
						</div>

						<form action="#">

							<div class="input-box">
								<input class="newslatter" type="email" id="email" placeholder="<?=strip_tags($this->translation['your_email'])?>" name="newlatter">
								<button title="Subscribe" type="submit" class="button" id="subscribe_btn"><i class="fa fa-paper-plane-o"></i>
								</button>
							</div>
							<div class="alert_msg hide"></div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--footer area are end-->

	<!--Copyright area are start-->
	<div class="copyright-info">
		<div class="container">
			<div class="row">
				<div class="col-sm-6 text-left col-xs-12 footer_txt">
					<?=date('Y')?>-All rights reserved.
				</div>
				<div class="col-sm-6 text-right col-xs-12 footer_txt">
					<a href="http://voodoo.pro/" target="_blank">
                    <span class="voodoo">
                        Powered By VooDoo programming
                        <div>
							<img src="<?=Yii::app()->request->BaseUrl?>/vendor/img/VooDooprogramming.png" alt="voodoo programming"
								 class="img-responsive">
						</div>
                    </span>
					</a>
				</div>
			</div>
		</div>
	</div>

	<!--Copyright area are end-->
</div>

<script>

	$('#subscribe_btn').click(function(e){
		e.preventDefault();

		var email = $('#email').val();
		$.ajax({
			url:'<?=$this->CreateUrl('site/subscribe')?>',
			data:{email:email},
			type:'POST',
			dataType:'JSON',
			success:function(result){
				$('.alert_msg').removeClass('hide');
				$('.alert_msg').html(result);
			}
		})
	});



	var height_to_gallery = function() {
		var main_slider = $('.slider-container').height();
		var halls = $('#halls').height();
		var our_team = $('#our_team').height();
		var section_menu = $('#section_menu').height();
		var gallery = $('#gallery').height();

		return main_slider+halls+our_team+section_menu+gallery;
	}

</script>
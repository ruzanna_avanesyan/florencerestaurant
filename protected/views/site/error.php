<html>

<head>
	<meta charset="utf-8">
	<title>404! Page not found</title>
	<link rel="stylesheet" type="text/css" href="<?=Yii::app()->request->BaseUrl?>/vendor/css/error.css">
</head>

<body>
<div class="home_page" >
	<a href="<?=$this->CreateUrl('site/index')?>" class="go_home" > <p>Home</p> </a>
</div>
<div class="image_cont">
	<img src="<?=Yii::app()->request->BaseUrl?>/vendor/img/errorpage/image.png" class="image" alt="error image">
</div>
</body>

</html>
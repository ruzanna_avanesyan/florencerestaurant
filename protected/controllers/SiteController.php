<?php

class SiteController extends Controller
{
	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{

		$criteria = new CDbCriteria();
		$criteria->order = 'sort_order ASC';
		$criteria->with  = array(
			'menuLabels' => array(
				'alias' 	=> 't1',
				'condition'	=> 't1.language_id = '.$this->langId
			)
		);
		$menu = Menu::model()->findAll($criteria);

		$follow_us = FollowUs::model()->findAllByAttributes(array(),array('order' => 'sort_order ASC'));
		$criteria  = new CDbCriteria();
		$criteria->with = array(
			'contactUsLabels' => array(
				'alias' 	=> 't1',
				'condition'	=> 't1.language_id = '.$this->langId
			)
		);
		$contacts = ContactUs::model()->find($criteria);

		$criteria = new CDbCriteria();
		$criteria->order 	= "sort_order ASC";
		$criteria->with		= array(
			'sliderLabels' 	=> array(
				'alias'		=> 't1',
				'condition'	=> 't1.language_id = '.$this->langId
			)
		);
		$slider = Slider::model()->findAll($criteria);


		$aboutUsPics = AboutUsPics::model()->findAll();

		$aboutUs = AboutUs::model()->with(array('aboutUsLabel'=>array('condition' => 'language_id = '.$this->langId)))->find();

		$criteria = new CDbCriteria();
		$criteria->order 	= 'sort_order ASC';
		$criteria->with 	= array(
			'collectiveLabel' => array(
				'alias'			=> 't1',
				'condition'		=> 't1.language_id = '.$this->langId
			),
			'collectiveExperiences'
		);
		$collective = Collective::model()->findAll($criteria);

		$restaurant_type = RestaurantTypes::model()->with(array('restaurantTypesLabel'=>array('condition' => 'language_id = '.$this->langId)))->findAll();

		$criteria = new CDbCriteria();
		$criteria->order = 'sort_order ASC';
		$criteria->with = array(
			'foodMenuLabels' => array(
				'alias' 	=> 't1',
				'condition'	=> 't1.language_id = '.$this->langId
			)
		);
		$food_menu = FoodMenu::model()->findAll($criteria);
//		$food_menu = FoodMenu::model()->with('foodMenuLabels',array('condition' => 'language_id = '.$this->langId))->findAllByAttributes(array(),array('order' => 'sort_order ASC'));

		$gallery_category = GalleryCategory::model()->with('galleryCategoryLabel',array('condition' => 'language_id = '.$this->langId))->findAllByAttributes(array(),array('order' => 'sort_order ASC'));
		$gallery = array();
		if(!empty($gallery_category)) {
			foreach ($gallery_category as $key => $value) {
				$criteria = new CDbCriteria();
				$criteria->order = 't.sort_order ASC';
				$criteria->limit = 6;
				$criteria->condition = 'category_id = '.$value->id;
				$gallery[$value->id] = Gallery::model()->findAll($criteria);
			}
		}
		$all_gallery = Gallery::model()->findAllByAttributes(array(),array('order' => 't.sort_order ASC','limit' => 6));


		$criteria = new CDbCriteria();
		$criteria->condition = 'date >= :date';
		$criteria->params = array(
			':date' => date('Y-m-d H:i:s',strtotime('-10 days'))
		);
		$criteria->group = 'DATE(date)';
		$criteria->order = 'date ASC';
		$events_days = Events::model()->findAll($criteria);

		$criteria = new CDbCriteria();
		$criteria->condition = 'date >= :date';
		$criteria->params = array(
			':date' => date('Y-m-d H:i:s',strtotime('-10 days'))
		);
		$criteria->order = 'date ASC';
		$criteria->with = array(
			'eventsLabel' => array(
				'alias' 	=> 't1',
				'condition'	=> 't1.language_id = '.$this->langId
			)
		);
		$events = Events::model()->findAll($criteria);

		$marker = Markers::model()->find();

		$openingKeys = OpeningKeys::model()->with('openingKeysLabel',array('condition' => array('language_id' => $this->langId)))->findAllByAttributes(array(),array('order' => 'sort_order ASC'));

		$data = array(
			'menu'			=> $menu,
			'follow_us'		=> $follow_us,
			'contacts'		=> $contacts,
			'slider'		=> $slider,
			'aboutUsPics'	=> $aboutUsPics,
			'aboutUs'		=> $aboutUs,
			'collective'	=> $collective,
			'res_type'		=> $restaurant_type,
			'food_menu'		=> $food_menu,
			'gal_category'	=> $gallery_category,
			'gallery'		=> $gallery,
			'all_gallery'	=> $all_gallery,
			'events_days'	=> $events_days,
			'events'		=> $events,
			'marker'		=> $marker,
			'opening_keys'	=> $openingKeys,
		);
//		Helpers::dump($contacts);die;

		$this->render('index',array('data' => $data));
	}

	public function actionMarkers(){

		$markers = Markers::model()->findAll();
		$return_value = array();
		if(!empty($markers)){
			foreach($markers as $key => $value){
				$return_value[$key]['lat'] = $value->lat;
				$return_value[$key]['lng'] = $value->lng;
			}
		}
		echo json_encode($return_value);die;
	}

	public function actionSubscribe(){
		if(isset($_POST['email'])){
			$result = '';
			if (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL) === false) {
				$subscribe_exist = Subscribe::model()->findByAttributes(array('email'=>$_POST['email']));
				if(empty($subscribe_exist)){
					$model = new Subscribe();
					$model->email = $_POST['email'];
					$model->created_date = Helpers::date();
					if($model->save()){
						$result = $this->translation['successfuly_subscribe'];
					}
				}else{
					$result = $this->translation['subscribe_busy'];
				}
			} else {
				$result = $this->translation['email_not_valid'];

			}
			echo json_encode($result);die;
		}
	}

	public function actionUnSubscribe(){
		if(isset($_GET['email'])){
			Subscribe::model()->deleteAllByAttributes(array('email' => strip_tags($_GET['email'])));
			$this->redirect('index');
		}
	}

	public function actionGetImages(){
		if(isset($_POST['category']) && isset($_POST['offset'])){
			$limit = 6;
			if($_POST['category'] == "all"){
				$gallery = Gallery::model()->findAllByAttributes(array(),array('order' => 'sort_order ASC','offset' => ($_POST['offset']*$limit),'limit' => $limit));
				$next_step = Gallery::model()->findAllByAttributes(array(),array('order' => 'sort_order ASC','offset' => (($_POST['offset']+1)*$limit),'limit' => $limit));

			}elseif(is_numeric($_POST['category'])){
				$gallery = Gallery::model()->findAllByAttributes(array('category_id' => $_POST['category']),array('order' => 'sort_order ASC','offset' => ($_POST['offset']*$limit),'limit' => $limit));
				$next_step = Gallery::model()->findAllByAttributes(array('category_id' => $_POST['category']),array('order' => 'sort_order ASC','offset' => (($_POST['offset']+1)*$limit),'limit' => $limit));
			}

			$return_value = array();
			if(empty($next_step)){
				$return_value['next_step'] = false;
			}else{
				$return_value['next_step'] = true;
			}
			$return_value['offset'] = $_POST['offset']+1;
			if(!empty($gallery)){
				foreach($gallery as $key => $value){
					$return_value['gallery'][$key]['id'] = $value->id;
					$return_value['gallery'][$key]['category_id'] = $value->category_id;
					$return_value['gallery'][$key]['image'] = $value->image;
				}
			}
			echo json_encode($return_value);die;
		}
	}
	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			$this->render('error', $error);die;
			if(Yii::app()->request->isAjaxRequest)

				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}


//	public function actionRoute(){
//		$this->layout = false;
//		$this->render('route');
//	}

	public function actionRoute(){
		$this->layout = false;
		$this->render('rout_by_lat');
	}


	/**
	 * Displays the contact page
	 */
	public function actionContact()
	{
		$model=new ContactForm;
		if(isset($_POST['ContactForm']))
		{
			$model->attributes=$_POST['ContactForm'];
			if($model->validate())
			{
				$name='=?UTF-8?B?'.base64_encode($model->name).'?=';
				$subject='=?UTF-8?B?'.base64_encode($model->subject).'?=';
				$headers="From: $name <{$model->email}>\r\n".
					"Reply-To: {$model->email}\r\n".
					"MIME-Version: 1.0\r\n".
					"Content-Type: text/plain; charset=UTF-8";

				mail(Yii::app()->params['adminEmail'],$subject,$model->body,$headers);
				Yii::app()->user->setFlash('contact','Thank you for contacting us. We will respond to you as soon as possible.');
				$this->refresh();
			}
		}
		$this->render('contact',array('model'=>$model));
	}

	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
		$model=new LoginForm;

		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if(isset($_POST['LoginForm']))
		{
			$model->attributes=$_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->login())
				$this->redirect(Yii::app()->user->returnUrl);
		}
		// display the login form
		$this->render('login',array('model'=>$model));
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}
}